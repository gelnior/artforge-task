package fr.hd3d.taskmanager.ui.client;

import com.google.gwt.core.client.EntryPoint;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.taskmanager.ui.client.view.TaskManagerView;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class TaskManager implements EntryPoint
{

    /** This is the entry point method. It sets up the controllers and raise the initialization event. */
    public void onModuleLoad()
    {
        @SuppressWarnings("unused")
        TaskManagerView view = new TaskManagerView();
        EventDispatcher.forwardEvent(CommonEvents.START);
    }
}
