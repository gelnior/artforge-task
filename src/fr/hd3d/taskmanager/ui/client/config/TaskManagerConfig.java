package fr.hd3d.taskmanager.ui.client.config;

/**
 * Specific constants for task manager.
 * 
 * @author HD3D
 */
public class TaskManagerConfig
{
    public static final String SHEET_COOKIE_VAR_PREFIX = "task-sheet-";
    public static final String PERSON_MENU_EVENT_VAR_NAME = "person-menu";
    public static final String APPROVAL_FIELD_EVENT_VAR = "approval";
    public static final String PROJECT_ALL_COOKIE_VALUE = "all";
}
