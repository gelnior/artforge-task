package fr.hd3d.taskmanager.ui.client.controller;

import java.util.List;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.RootPanel;

import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.dialog.CommentTaskDialog;
import fr.hd3d.common.ui.client.widget.explorer.config.ExplorerConfig;
import fr.hd3d.common.ui.client.widget.explorer.controller.ExplorerController;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.MainController;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.SheetEditorDisplayer;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.taskmanager.ui.client.config.TaskManagerConfig;
import fr.hd3d.taskmanager.ui.client.events.TaskManagerEvents;
import fr.hd3d.taskmanager.ui.client.model.TaskManagerModel;
import fr.hd3d.taskmanager.ui.client.view.ExportOdsController;
import fr.hd3d.taskmanager.ui.client.view.TaskManagerView;


/**
 * Task manager controller handles task manager events.
 * 
 * @author HD3D
 */
public class TaskManagerController extends MainController
{
    /** Model that handles task manager data. */
    private final TaskManagerModel model;
    /** View that handles task manager widgets. */
    private final TaskManagerView view;

    private boolean isFirstSheetLoading = false;

    public static FormPanel exportOdsFormPanel;

    /**
     * Constructor
     * 
     * @param view
     *            Model that handles task manager data.
     * @param model
     *            View that handles task manager widgets.
     */
    public TaskManagerController(TaskManagerView view, TaskManagerModel model)
    {
        super(model, view);

        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);

        EventType type = event.getType();

        // Application context changes
        if (type == TaskManagerEvents.PROJECT_CHANGED)
        {
            this.onProjectChanged(event);
        }
        else if (type == TaskManagerEvents.SHEET_CHANGED)
        {
            this.onSheetChanged(event);
        }
        else if (type == CommonEvents.CONSTITUENTS_SELECTED)
        {
            this.onConstituentSelected(event);
        }
        else if (type == CommonEvents.SHOTS_SELECTED)
        {
            this.onShotSelected(event);
        }
        else if (type == CommonEvents.CATEGORIES_SELECTED)
        {
            this.onCategorySelected(event);
        }
        else if (type == CommonEvents.SEQUENCES_SELECTED)
        {
            this.onSequenceSelected(event);
        }

        // Sheet events
        else if (type == SheetEditorEvents.NEW_SHEET_CLICKED)
        {
            this.onNewSheetClicked();
        }
        else if (type == SheetEditorEvents.EDIT_SHEET_CLICKED)
        {
            this.onEditSheetClicked();
        }
        else if (type == SheetEditorEvents.END_SAVE)
        {
            this.onSheetSaved(event);
        }
        else if (type == SheetEditorEvents.DELETE_SHEET_CLICKED)
        {
            this.onDeleteSheetClicked();
        }
        else if (type == TaskManagerEvents.SHEET_DELETED)
        {
            this.onSheetDeleted();
        }

        // Explorer events
        else if (type == ExplorerEvents.BEFORE_SAVE)
        {
            this.onBeforeExplorerSave();
        }
        else if (type == ExplorerEvents.AFTER_SAVE)
        {
            this.afterExplorerSave();
        }
        else if (type == ExplorerEvents.NEW_GRID_BUILT)
        {
            this.onNewGridBuilt();
        }
        else if (type == ExplorerEvents.SELECT_CHANGE)
        {
            this.onSelectChange();
        } 
        else if (type == ExplorerEvents.DATA_LOADED) 
        {
        	this.onExplorerDataLoaded();
        }
        else if (type == TaskManagerEvents.CREATE_MENU_ASSIGNATION)
        {
            this.onCreateMenuAssignation(event);
        }
        else if (type == TaskManagerEvents.UPDATE_CELL_STATUS)
        {
            this.onUpdateCellStatus(event);
        }
        else if (type == TaskManagerEvents.UPDATE_CELL_DURATION)
        {
            this.onUpdateCellDuration(event);
        }
        else if (type == TaskManagerEvents.UPDATE_CELL_WORKER)
        {
            this.onUpdateCellWorker(event);
        }
        else if (type == TaskManagerEvents.APPROVAL_DOUBLE_CLICKED)
        {
            this.onApprovalDoubleClicked(event);
        }

        // Task edition
        else if (type == TaskManagerEvents.CREATE_NEW_TASK_CONSTITUENT_CLICKED)
        {
            this.onCreateConstituentTaskClicked();
        }
        else if (type == TaskManagerEvents.CREATE_NEW_TASK_SHOT_CLICKED)
        {
            this.onCreateShotTaskClicked();
        }
        else if (type == TaskManagerEvents.TASK_CREATED)
        {
            this.onTaskCreated();
        }
        else if (type == TaskManagerEvents.DELETE_TASK_CONFIRMED)
        {
            this.onDeleteTaskConfirmed();
        }
        else if (type == TaskManagerEvents.DELETE_TASK_CLICKED)
        {
            this.onDeleteTaskClicked();
        }
        else if (type == TaskManagerEvents.DELETE_TASK_SUCCESS)
        {
            this.onDeleteTaskSuccess();
        }
        else if (type == TaskManagerEvents.PERSON_ASSIGNED)
        {
            this.onPersonAssigned(event);
        }

        // Task type events
        else if (type == CommonEvents.TASK_TYPE_BUTTON_CLICKED)
        {
            this.onTaskTypeButtonClicked();
        }
        else if (type == TaskManagerEvents.UPDATE_CELL_STATUS_CONFIRMED)
        {
            this.onUpdateCellStatusConfirmed(event);
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    private void onExplorerDataLoaded() {
		this.view.enableTrees();
	}

	private void onUpdateCellStatusConfirmed(AppEvent event)
    {
        String comment = event.getData(CommentTaskDialog.COMMENT_DATA);
        List<Hd3dModelData> taskList = event.getData(CommentTaskDialog.TASKS_DATA);
        if (taskList != null)
        {
            for (Hd3dModelData task : taskList)
            {
                task.set(ItemModelData.EXTRA_FIELD, comment);
            }
        }

        if (ExplorerController.autoSave)
            EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA, Boolean.TRUE);
    }

    /**
     * When settings are initialized, excluded fields are set then widgets are initiazed and configured.
     */
    @Override
    protected void onSettingInitialized(AppEvent event)
    {
        this.model.setExcludedFields();
        this.setContext();

        this.view.hideStartPanel();
        this.view.initTaskManagerView();
        this.addChild(this.view.getTaskTypeEditorController());

        this.createExportOdsFormPanel();
        ExportOdsController exportOdsController = new ExportOdsController(this.model, this.view);
        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.addController(exportOdsController);

        this.view.loadProjectComboBoxData();
    }

    /**
     * Rebuild last user context.
     */
    private void setContext()
    {
        Boolean isAutoSave = UserSettings.getSettingAsBoolean(ExplorerConfig.SETTING_AUTOSAVE);
        ExplorerController.setAutoSave(isAutoSave);
        if (ExplorerController.getAutoSave())
            this.view.toggleAutoSave();
    }

    /**
     * When project changed, the new project is registered and all widgets data are reloaded. If All project is
     * selected, navigation widgets are disabled.
     * 
     * @param project
     *            The selected project.
     */
    private void onProjectChanged(AppEvent event)
    {
        ProjectModelData project = event.getData();

        if (project != null && !project.getId().toString().equals("-1"))
        {
            this.model.setCurrentProjectSelected(project);
            this.model.setIsProjectLoaded(true);
            this.isFirstSheetLoading = true;
            this.view.reloadSheets(project);
            this.view.reloadTrees(project);
            this.view.enableTrees();
        }
        else
        {
            this.model.setCurrentProjectSelected(null);
            this.model.setIsProjectLoaded(false);
            this.view.reloadSheets(null);
            this.view.clearTrees();
        }

        this.saveMainProjectToCookie();
        this.model.reloadAssignationStore(project);
    }

    /**
     * Set, inside favorite cookie, the currently selected project as the last selected project. Modify also sheet
     * cookie key for sheet combo box context saving.
     */
    private void saveMainProjectToCookie()
    {
        String cookieKey = TaskManagerConfig.SHEET_COOKIE_VAR_PREFIX + "-";
        if (MainModel.getCurrentProject() != null)
            cookieKey += MainModel.getCurrentProject().getId();
        else
            cookieKey += TaskManagerConfig.PROJECT_ALL_COOKIE_VALUE;
        this.view.setSheetCookie(cookieKey);
    }

    /**
     * When selected sheet changed, it updates all explorer data (filter and explorer grid data) and it saves sheet ID
     * to favorite cookie (to auto-select this sheet when the corresponding project will be loaded).
     * 
     * @param event
     *            The sheet changed event.
     */
    private void onSheetChanged(AppEvent event)
    {
        SheetModelData sheet = event.getData();

        this.view.disableDeleteButton();
        this.view.clearSheetFilter();
        if (sheet != null)
        {
            this.saveSheetToCookie(sheet);
            this.view.setExplorerSheet(sheet, !this.isFirstSheetLoading);
            if (this.isFirstSheetLoading)
                this.isFirstSheetLoading = false;
            this.view.refreshExplorerFilter();
        }
        else
        {
            this.model.getExplorerStore().removeAll();
        }
    }

    /**
     * Save, inside favorite cookie, for current project, the ID of the sheet given in parameter.
     */
    private void saveSheetToCookie(SheetModelData sheet)
    {
        String cookieKey = TaskManagerConfig.SHEET_COOKIE_VAR_PREFIX + "-";
        if (MainModel.getCurrentProject() != null)
            cookieKey += MainModel.getCurrentProject().getId();
        else
            cookieKey += TaskManagerConfig.PROJECT_ALL_COOKIE_VALUE;
        FavoriteCookie.putFavValue(cookieKey, sheet.getId().toString());
    }

    /**
     * When constituents are selected, it reloads explorer to display tasks linked to these constituents.
     * 
     * @param event
     *            Constituent selected event.
     */
    private void onConstituentSelected(AppEvent event)
    {
        this.view.removeParameterFromExplorer(this.model.getSequenceConstraint());
        this.view.removeParameterFromExplorer(this.model.getCategoryConstraint());
        this.view.clearShotSelection();

        List<ConstituentModelData> constituents = event.getData();
        if (CollectionUtils.isNotEmpty(constituents))
        {
            this.model.setConstituentFilter(constituents);
            this.view.disableDeleteButton();
            this.view.disableTrees();
            this.view.reloadExplorerData();
        }
    }

    /**
     * When shots are selected, it reloads explorer to display tasks linked to these shots.
     * 
     * @param event
     *            Constituent selected event.
     */
    private void onShotSelected(AppEvent event)
    {
        this.view.removeParameterFromExplorer(this.model.getSequenceConstraint());
        this.view.removeParameterFromExplorer(this.model.getCategoryConstraint());
        this.view.clearConstituentSelection();

        List<ShotModelData> shots = event.getData();
        if (CollectionUtils.isNotEmpty(shots))
        {
            this.model.setShotFilter(shots);
            this.view.disableDeleteButton();
            this.view.disableTrees();
            this.view.reloadExplorerData();
        }
    }

    /**
     * When categories are selected, it reloads explorer to display tasks linked to constituents of these categories.
     * 
     * @param event
     *            Constituent selected event.
     */
    private void onCategorySelected(AppEvent event)
    {
        this.view.removeParameterFromExplorer(this.model.getCategoryConstraint());
        this.view.removeParameterFromExplorer(this.model.getSequenceConstraint());
        this.view.clearShotSelection();

        List<CategoryModelData> categories = event.getData();
        if (CollectionUtils.isNotEmpty(categories))
        {
            CategoryModelData category = categories.get(0);
            if (category.getId() != -1)
            {
                this.model.setCategoryFilter(categories);
                this.view.addParameterToExplorer(this.model.getCategoryConstraint());
            }
            else
            {
                this.model.setAllConstituents();
            }
            this.view.disableTrees();
            this.view.reloadExplorerData();
        }
    }

    /**
     * When sequences are selected, it reloads explorer to display tasks linked to sequences of these sequences.
     * 
     * @param event
     *            Constituent selected event.
     */
    private void onSequenceSelected(AppEvent event)
    {
        this.view.removeParameterFromExplorer(this.model.getSequenceConstraint());
        this.view.removeParameterFromExplorer(this.model.getCategoryConstraint());
        this.view.clearConstituentSelection();

        List<SequenceModelData> sequences = event.getData();
        if (CollectionUtils.isNotEmpty(sequences))
        {
            SequenceModelData sequence = sequences.get(0);
            if (sequence.getId() != -1)
            {
                this.model.setSequenceFilter(sequences);
                this.view.addParameterToExplorer(this.model.getSequenceConstraint());
            }
            else
            {
                this.model.setAllShots();
            }
            this.view.disableTrees();
            this.view.reloadExplorerData();
        }
    }

    /**
     * When new sheet is clicked, sheet editor window is built then it is displayed and configured for current project.
     */
    private void onNewSheetClicked()
    {
        if (SheetEditorDisplayer.isNull())
        {
            SheetEditorDisplayer.createSheetEditorWindow(this.model.getEntities());
            SheetEditorDisplayer.hideValidationButton();
        }

        if (MainModel.currentProject != null)
        {
            SheetEditorDisplayer.showEditor(MainModel.currentProject.getId());
        }
        else
        {
            SheetEditorDisplayer.showEditor();
        }
    }

    /**
     * When edit sheet is clicked, sheet editor window is built then it is displayed and configured for current sheet.
     */
    private void onEditSheetClicked()
    {
        if (SheetEditorDisplayer.isNull())
        {
            SheetEditorDisplayer.createSheetEditorWindow(this.model.getEntities());
            SheetEditorDisplayer.hideValidationButton();
        }

        SheetEditorDisplayer.showEditor(this.view.getSelectedSheet());
    }

    /**
     * When sheet is saved, sheet combo box is updated by adding it when it is a new sheet, or updating it if sheet
     * already exists. Then this sheet is selected.
     * 
     * @param event
     *            Sheet saved event.
     */
    private void onSheetSaved(AppEvent event)
    {
        SheetModelData sheet = (SheetModelData) event.getData();
        ServiceStore<SheetModelData> store = this.model.getSheetStore();
        if (store.indexOf(sheet) == -1)
        {
            store.add(sheet);
            this.view.selectSheet(sheet);
        }
        else
        {
            store.update(sheet);
            Record rec = store.getRecord(sheet);
            rec.set(SheetModelData.NAME_FIELD, sheet.getName());
            this.view.setExplorerSheet(sheet, true);
        }
    }

    /**
     * When delete view button is clicked, it deletes from server the current view. Once the view is deleted it forwards
     * the event sheet deleted. Which has for effect to remove the view from combo store.
     */
    private void onDeleteSheetClicked()
    {
        SheetModelData view = this.view.getSelectedSheet();
        if (view != null)
        {
            this.view.enableSheetComboBox(false);
            (view).delete(TaskManagerEvents.SHEET_DELETED);
        }
    }

    /**
     * After a sheet has been deleted from server, it removes it from the view combo store then it selects the first
     * sheet in the store.
     */
    private void onSheetDeleted()
    {
        this.model.getSheetStore().remove(this.view.getSelectedSheet());
        this.view.enableSheetComboBox(true);
        this.view.selectFirstSheet();
    }

    /**
     * When explorer starts saving it displays saving indicator.
     */
    private void onBeforeExplorerSave()
    {
        this.view.showSaving();
    }

    /**
     * When explorer ends saving it displays saving indicator.
     */
    private void afterExplorerSave()
    {
        this.view.hideSaving();
    }

    /**
     * When new explorer grid is built, explorer task manager specific listeners are set on it.
     */
    private void onNewGridBuilt()
    {
        this.view.setExplorerListener();
    }

    /**
     * When selection changes, it enables or disables delete task button depending of the fact that selection is empty
     * or not.
     */
    private void onSelectChange()
    {
        if (this.view.getSelectedTasks().size() > 0)
        {
            this.view.enableDeleteButton();
        }
        else
        {
            this.view.disableDeleteButton();
        }
    }

    /**
     * When an approval note cell is double clicked it hides it expands or collapse it (display its history or not).
     * 
     * @param event
     *            Approval note double clicked event.
     */
    private void onApprovalDoubleClicked(AppEvent event)
    {
        String field = event.getData(TaskManagerConfig.APPROVAL_FIELD_EVENT_VAR);
        List<Hd3dModelData> selection = this.view.getSelectedTasks();

        for (Hd3dModelData task : selection)
        {
            List<ApprovalNoteModelData> approvals = task.get(field);
            if (CollectionUtils.isNotEmpty(approvals) && approvals.get(0).getId() != null)
            {
                Long id = approvals.get(0).getId();
                this.view.hideOrDisplayApprovalChild(id);
            }
        }
    }

    /**
     * When create task is clicked for a constituent, it displays task creator dialog for this constituent.
     */
    private void onCreateConstituentTaskClicked()
    {
        List<RecordModelData> constituent = this.view.getSelectedConstituents();
        this.view.displayTaskCreator(constituent);
    }

    /**
     * When create task is clicked for shto, it displays task creator dialog for this shot.
     */
    private void onCreateShotTaskClicked()
    {
        List<RecordModelData> shot = this.view.getSelectedShots();
        this.view.displayTaskCreator(shot);
    }

    /**
     * When a task is created it reloads explorer data.
     */
    private void onTaskCreated()
    {
        this.view.reloadExplorerData();
    }

    /**
     * When delete task is clicked, if tasks are selected, it demands for confirmation before deleting selected tasks.
     */
    private void onDeleteTaskClicked()
    {
        if (this.view.getSelectedTasks().size() > 0)
        {
            this.view.askForDeleteConfirmation();
        }
    }

    /**
     * When task deletion is confirmed, it shows saving indicator and ask to model for task deletion.
     */
    private void onDeleteTaskConfirmed()
    {
        List<Hd3dModelData> tasks = this.view.getSelectedTasks();
        this.model.setNbTaskToDelete(tasks.size());
        this.view.showSaving();
        for (Hd3dModelData task : tasks)
        {
            this.model.deleteTask(task);
        }
    }

    /**
     * When task is successfully deleted, it hides saving indicator and reloads explorer data.
     */
    private void onDeleteTaskSuccess()
    {
        this.model.decNbTaskToDelete();
        if (this.model.getNbTaskToDelete() <= 0)
        {
            this.view.hideSaving();
            this.view.reloadExplorerData();
            // EventDispatcher.forwardEvent(ExplorerEvents.RELOAD_DATA);
        }
    }

    /**
     * When a new person is assigned via person combo box, this person is automatically added to the project.
     * 
     * @param event
     */
    private void onPersonAssigned(AppEvent event)
    {
        PersonModelData person = event.getData();
        if (person != null && MainModel.getCurrentProject() != null
                && this.model.getAssignationStore().findModel(PersonModelData.ID_FIELD, person.getId()) == null)
        {
            this.model.addPersonToCurrentProjectGroup(person);
        }
    }

    /* Work around should be rewritten. */
    private void onUpdateCellStatus(AppEvent event)
    {
        String status = event.getData();

        for (Hd3dModelData task : this.view.getSelectedTasks())
        {
            Record rd = this.model.getExplorerStore().getRecord(task);
            rd.set(TaskModelData.STATUS_FIELD, status);
            rd.setDirty(true);
        }

        this.view.openCommentTaskDialog();
    }

    /* Work around should be rewritten. */
    private void onUpdateCellDuration(AppEvent event)
    {
        Grid<Hd3dModelData> grid = event.getData("GRID");
        Long duration = event.getData("DURATION");

        for (Hd3dModelData task : this.view.getSelectedTasks())
        {
            Record rd = grid.getStore().getRecord(task);
            rd.set("duration", duration);
            rd.setDirty(true);
        }
        if (ExplorerController.autoSave)
            EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA, Boolean.TRUE);
    }

    /* Work around should be rewritten. */
    private void onUpdateCellWorker(AppEvent event)
    {
        PersonModelData worker = event.getData();

        for (Hd3dModelData task : this.view.getSelectedTasks())
        {
            Record rd = this.model.getExplorerStore().getRecord(task);
            rd.set("worker", worker);
            rd.setDirty(true);
        }

        if (ExplorerController.autoSave)
            EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA, Boolean.TRUE);
    }

    /**
     * When task type button is clicked, it displays the task type editor.
     */
    private void onTaskTypeButtonClicked()
    {
        this.view.displayTaskTypeEditor();
    }

    /**
     * When error occurs it displays the corresponding message and hides explorer saving indicator.
     */
    @Override
    protected void onError(AppEvent event)
    {
        super.onError(event);
        this.view.hideSaving();
    }

    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(TaskManagerEvents.PROJECT_CHANGED);
        this.registerEventTypes(TaskManagerEvents.SHEET_CHANGED);
        this.registerEventTypes(CommonEvents.CONSTITUENTS_SELECTED);
        this.registerEventTypes(CommonEvents.SHOTS_SELECTED);
        this.registerEventTypes(CommonEvents.CATEGORIES_SELECTED);
        this.registerEventTypes(CommonEvents.SEQUENCES_SELECTED);

        this.registerEventTypes(SheetEditorEvents.NEW_SHEET_CLICKED);
        this.registerEventTypes(SheetEditorEvents.EDIT_SHEET_CLICKED);
        this.registerEventTypes(SheetEditorEvents.END_SAVE);
        this.registerEventTypes(SheetEditorEvents.DELETE_SHEET_CLICKED);
        this.registerEventTypes(TaskManagerEvents.SHEET_DELETED);

        this.registerEventTypes(TaskManagerEvents.CREATE_NEW_TASK_CONSTITUENT_CLICKED);
        this.registerEventTypes(TaskManagerEvents.CREATE_NEW_TASK_SHOT_CLICKED);
        this.registerEventTypes(TaskManagerEvents.TASK_CREATED);
        this.registerEventTypes(TaskManagerEvents.DELETE_TASK_CLICKED);
        this.registerEventTypes(TaskManagerEvents.DELETE_TASK_CONFIRMED);
        this.registerEventTypes(TaskManagerEvents.DELETE_TASK_SUCCESS);
        this.registerEventTypes(CommonEvents.TASK_TYPE_BUTTON_CLICKED);

        this.registerEventTypes(ExplorerEvents.SELECT_CHANGE);
        this.registerEventTypes(ExplorerEvents.RELOAD_DATA);
        this.registerEventTypes(ExplorerEvents.BEFORE_SAVE);
        this.registerEventTypes(ExplorerEvents.AFTER_SAVE);
        this.registerEventTypes(ExplorerEvents.NEW_GRID_BUILT);
        this.registerEventTypes(ExplorerEvents.DATA_LOADED);
        this.registerEventTypes(TaskManagerEvents.APPROVAL_DOUBLE_CLICKED);

        this.registerEventTypes(TaskManagerEvents.CREATE_MENU_ASSIGNATION);
        this.registerEventTypes(TaskManagerEvents.UPDATE_CELL_STATUS);
        this.registerEventTypes(TaskManagerEvents.UPDATE_CELL_STATUS_CONFIRMED);
        this.registerEventTypes(TaskManagerEvents.UPDATE_CELL_DURATION);
        this.registerEventTypes(TaskManagerEvents.UPDATE_CELL_WORKER);
        this.registerEventTypes(TaskManagerEvents.UPDATE_CELL_WORKER);

        this.registerEventTypes(TaskManagerEvents.PERSON_ASSIGNED);
    }

    /* Work around should be rewritten soon. */
    LoadListener listener = new LoadListener() {
        @Override
        public void loaderLoad(LoadEvent le)
        {
            onRefreshAssingationMenu();
        }
    };

    /* Work around should be rewritten soon. */
    Menu menu;

    /* Work around should be rewritten soon. */
    private void onRefreshAssingationMenu()
    {
        model.getAssignationStore().removeLoadListener(listener);
        AppEvent event = new AppEvent(TaskManagerEvents.CREATE_MENU_ASSIGNATION);
        event.setData(TaskManagerConfig.PERSON_MENU_EVENT_VAR_NAME, menu);
        onCreateMenuAssignation(event);
    }

    /* Work around should be rewritten soon. */
    public void onCreateMenuAssignation(AppEvent event)
    {
        menu = event.getData(TaskManagerConfig.PERSON_MENU_EVENT_VAR_NAME);
        MenuItem menuItem = new MenuItem("<i>Unassigned</i>");
        menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                AppEvent event = new AppEvent(TaskManagerEvents.UPDATE_CELL_WORKER);
                EventDispatcher.forwardEvent(event);
            }
        });
        menu.add(menuItem);

        for (final PersonModelData person : this.model.getAssignationStore().getModels())
        {
            String stringValue = person.getFullName();
            menuItem = new MenuItem(stringValue);
            menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    AppEvent event = new AppEvent(TaskManagerEvents.UPDATE_CELL_WORKER, person);
                    EventDispatcher.forwardEvent(event);
                }
            });
            menu.add(menuItem);
        }

        menuItem = new MenuItem("Refresh person list");
        menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                // model.getAssignationStore().addLoadListener(listener);
                model.getAssignationStore().reload();
            }
        });

        menu.add(menuItem);

        menuItem = new MenuItem("<i>To add other person, use </i></br>" + "<i>Project or Team application</i>");
        menu.add(menuItem);
    }

    private void createExportOdsFormPanel()
    {
        exportOdsFormPanel = new FormPanel();
        exportOdsFormPanel.setMethod(FormPanel.METHOD_POST);
        RootPanel.get().add(exportOdsFormPanel);
    }
}
