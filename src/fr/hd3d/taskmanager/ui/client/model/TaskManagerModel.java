package fr.hd3d.taskmanager.ui.client.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.PersonReader;
import fr.hd3d.common.ui.client.modeldata.reader.ResourceGroupReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.AndConstraint;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.service.parameter.LogicLuceneConstraint;
import fr.hd3d.common.ui.client.service.parameter.LuceneConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.explorer.model.reader.SheetReader;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.taskmanager.ui.client.events.TaskManagerEvents;


/**
 * TaskManagerModel handles task manager data.
 * 
 * @author HD3D
 */
public class TaskManagerModel extends MainModel
{
    /** File to configure available renderer and editor in the sheet editor. */
    private static final String DATA_TYPE_FILE = "config/items_info_task.json";

    /** Store for available sheet for current project. */
    private ServiceStore<SheetModelData> sheetStore = new ServiceStore<SheetModelData>(new SheetReader());
    /** List of entity available in the task explorer (currently only tasks). */
    private List<EntityModelData> entities;
    /** Data store used for explorer. */
    private ServicesPagingStore<Hd3dModelData> explorerStore;

    /** The list of person available for assignation via context menu of worker renderer. */
    private final ServiceStore<PersonModelData> assignationStore = new ServiceStore<PersonModelData>(new PersonReader());

    /** True if project is already loaded. */
    private Boolean isProjectLoaded = false;
    /** True if sheet is loaded. */
    private Boolean isSheetLoaded = false;

    /** Root filter used to change explorer store filter. */
    private final AndConstraint rootFilter = new AndConstraint();

    /** The constraint to retrieve only tasks for current project. */
    private final EqConstraint projectConstraint = new EqConstraint("project.id");

    /** The constraint used to retrieve only task for a selected work object. */
    private final LogicConstraint entityLinkConstraint = new LogicConstraint(EConstraintLogicalOperator.AND);
    /** The constraint used to retrieve only task for given work object id. */
    private final InConstraint workObjectIdConstraint = new InConstraint("boundEntityTaskLinks.boundEntity");

    /** The constraint used to retrieve only task for given work type (constituent or shot). */
    private final EqConstraint workObjectEntityNameConstraint = new EqConstraint("boundEntityTaskLinks.boundEntityName");

    /** Constraint used to retrieve only work objects that are children of a given category. */
    private final LuceneConstraint categoryConstraintId = new LuceneConstraint(EConstraintOperator.in, "bound_par_ids");
    /** Constraint used to retrieve only work objects that are children of a given sequence. */
    private final LuceneConstraint sequenceConstraintId = new LuceneConstraint(EConstraintOperator.in, "bound_par_ids");

    /** Constraint used to retrieve only work objects that are constituents. */
    private final LogicLuceneConstraint categoryConstraint = new LogicLuceneConstraint(EConstraintLogicalOperator.AND);
    /** Constraint used to retrieve only work objects that are shots. */
    private final LogicLuceneConstraint sequenceConstraint = new LogicLuceneConstraint(EConstraintLogicalOperator.AND);

    /** The number of task to delete. */
    private int nbTaskToDelete;

    /**
     * Constructor : configure constraint used to display right data inside explorer.
     */
    public TaskManagerModel()
    {
        SheetEditorModel.addDataFile(DATA_TYPE_FILE);

        this.assignationStore.addParameter(new OrderBy(Arrays.asList(PersonModelData.PERSON_LAST_NAME_FIELD,
                PersonModelData.PERSON_FIRST_NAME_FIELD)));

        getFilter().setLeftMember(projectConstraint);
        entityLinkConstraint.setLeftMember(workObjectIdConstraint);
        entityLinkConstraint.setRightMember(workObjectEntityNameConstraint);

        categoryConstraint.setLeftMember(categoryConstraintId);
        categoryConstraint.setRightMember(new LuceneConstraint(EConstraintOperator.eq, "bound_name", "Constituent"));

        sequenceConstraint.setLeftMember(sequenceConstraintId);
        sequenceConstraint.setRightMember(new LuceneConstraint(EConstraintOperator.eq, "bound_name", "Shot"));

    }

    public ServicesPagingStore<Hd3dModelData> getExplorerStore()
    {
        return this.explorerStore;
    }

    public void setExplorerStore(ServicesPagingStore<Hd3dModelData> store)
    {
        this.explorerStore = store;
    }

    public ServiceStore<SheetModelData> getSheetStore()
    {
        return sheetStore;
    }

    public void setSheetStore(ServiceStore<SheetModelData> store)
    {
        this.sheetStore = store;
    }

    public void setEntities(List<EntityModelData> entities)
    {
        this.entities = entities;
    }

    public ServiceStore<PersonModelData> getAssignationStore()
    {
        return assignationStore;
    }

    public Boolean getIsProjectLoaded()
    {
        return isProjectLoaded;
    }

    public void setIsProjectLoaded(Boolean isProjectLoaded)
    {
        this.isProjectLoaded = isProjectLoaded;
    }

    public Boolean getIsSheetLoaded()
    {
        return isSheetLoaded;
    }

    public void setIsSheetLoaded(Boolean isSheetLoaded)
    {
        this.isSheetLoaded = isSheetLoaded;
    }

    public List<EntityModelData> getEntities()
    {
        return this.entities;
    }

    public int getNbTaskToDelete()
    {
        return this.nbTaskToDelete;
    }

    public void setNbTaskToDelete(int nbTaskToDelete)
    {
        this.nbTaskToDelete = nbTaskToDelete;
    }

    public void decNbTaskToDelete()
    {
        this.nbTaskToDelete--;
    }

    public LogicConstraint getFilter()
    {
        return this.rootFilter;
    }

    public LogicLuceneConstraint getCategoryConstraint()
    {
        return categoryConstraint;
    }

    public LogicLuceneConstraint getSequenceConstraint()
    {
        return sequenceConstraint;
    }

    /**
     * When currently selected project is set, it set first constraint to filter for having only tasks for this project.
     * If project is null it sets a dumb constraint to avoid empty filter (which causes request fails).
     * 
     * @param project
     *            The project to set.
     */
    public void setCurrentProjectSelected(ProjectModelData project)
    {
        MainModel.setCurrentProject(project);

        if (project != null)
        {
            this.projectConstraint.setLeftMember(project.getId());
            this.getFilter().setLeftMember(this.projectConstraint);
            this.getFilter().setRightMember(null);
        }
        else
        {
            this.getFilter().setLeftMember(new Constraint(EConstraintOperator.isnotnull, "name"));
        }
    }

    /**
     * Send a delete request to backend services and forwards a DELETE_TASK_SUCCESS event when request succeeds.
     * 
     * @param task
     *            The task to delete.
     */
    public void deleteTask(Hd3dModelData task)
    {
        task.setDefaultPath(getCurrentProject().getDefaultPath() + "/" + ServicesPath.TASKS + task.getId());
        task.delete(TaskManagerEvents.DELETE_TASK_SUCCESS);
    }

    /**
     * Set constraints to display only constituents that are children of given categories.
     * 
     * @param categories
     *            The categories to display.
     */
    public void setConstituentFilter(List<ConstituentModelData> constituentList)
    {
        if (CollectionUtils.isNotEmpty(constituentList))
        {
            this.getFilter().setLeftMember(this.projectConstraint);
            this.workObjectEntityNameConstraint.setLeftMember(ConstituentModelData.SIMPLE_CLASS_NAME);

            ArrayList<Long> idList = new ArrayList<Long>();
            for (ConstituentModelData constituent : constituentList)
            {
                idList.add(constituent.getId());
            }
            this.workObjectIdConstraint.setLeftMember(idList);

            this.getFilter().setRightMember(entityLinkConstraint);
        }
    }

    /**
     * Set constraints to display only given tasks of given shots.
     * 
     * @param shots
     *            The categories to display.
     */
    public void setShotFilter(List<ShotModelData> shots)
    {
        if (CollectionUtils.isNotEmpty(shots))
        {
            this.getFilter().setLeftMember(this.projectConstraint);
            this.workObjectEntityNameConstraint.setLeftMember(ShotModelData.SIMPLE_CLASS_NAME);
            ArrayList<Long> idList = new ArrayList<Long>();
            for (ShotModelData shot : shots)
            {
                idList.add(shot.getId());
            }
            this.workObjectIdConstraint.setLeftMember(idList);
            this.getFilter().setRightMember(entityLinkConstraint);
        }
    }

    /**
     * Set constraints to display only tasks of constituent that are children of given categories.
     * 
     * @param categories
     *            The categories to display.
     */
    public void setCategoryFilter(List<CategoryModelData> categories)
    {
        if (CollectionUtils.isNotEmpty(categories))
        {
            ArrayList<String> idList = new ArrayList<String>();
            for (CategoryModelData category : categories)
            {
                idList.add(category.getId().toString());
            }
            this.getFilter().setRightMember(null);

            this.categoryConstraintId.setLeftMember(idList);
        }
    }

    /**
     * Set constraints to display only tasks of shot that are children of given sequences.
     * 
     * @param sequences
     *            The sequences to display.
     */
    public void setSequenceFilter(List<SequenceModelData> sequences)
    {
        if (CollectionUtils.isNotEmpty(sequences))
        {
            ArrayList<String> idList = new ArrayList<String>();
            for (SequenceModelData sequence : sequences)
            {
                idList.add(sequence.getId().toString());
            }
            this.getFilter().setRightMember(null);

            this.sequenceConstraintId.setLeftMember(idList);
        }
    }

    /**
     * Set explorer store constraints to display all tasks of constituent for current project.
     */
    public void setAllConstituents()
    {
        this.getFilter().setLeftMember(this.projectConstraint);
        this.getFilter().setRightMember(workObjectEntityNameConstraint);
        this.workObjectEntityNameConstraint.setLeftMember(ConstituentModelData.SIMPLE_CLASS_NAME);
    }

    /**
     * Set explorer store constraints to display all tasks of shot for current project.
     */
    public void setAllShots()
    {
        this.getFilter().setLeftMember(this.projectConstraint);
        this.getFilter().setRightMember(workObjectEntityNameConstraint);
        this.workObjectEntityNameConstraint.setLeftMember(ShotModelData.SIMPLE_CLASS_NAME);
    }

    /**
     * Reload data to display for context menu assignation. It retrieves persons of the group linked to the project.
     * 
     * @param project
     *            The project for which people should be loaded.
     */
    public void reloadAssignationStore(ProjectModelData project)
    {

        if (project != null && project.getId() != -1)
        {
            this.assignationStore.setPath(ServicesPath.PROJECTS + project.getId() + "/" + ServicesPath.PERSONS);
        }
        else
        {
            this.assignationStore.setPath(ServicesPath.PERSONS);
        }

        this.assignationStore.reload();
    }

    /**
     * Retrieve groups of current project and add given person to the first of these groups.
     * 
     * @param person
     *            The person to add to current project main group.
     */
    public void addPersonToCurrentProjectGroup(final PersonModelData person)
    {
        final ServiceStore<ResourceGroupModelData> groups = new ServiceStore<ResourceGroupModelData>(
                new ResourceGroupReader());
        groups.setPath(MainModel.getCurrentProject().getDefaultPath() + "/" + ServicesPath.RESOURCE_GROUPS);

        groups.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent event)
            {
                if (groups.getCount() > 0)
                {
                    ResourceGroupModelData group = groups.getAt(0);
                    group.getResourceIds().add(person.getId());
                    group.save();
                }
            }
        });
        groups.reload();
    }

    /**
     * Give list of fields to not display in sheet editor.
     */
    public void setExcludedFields()
    {
        ExcludedField.addExcludedField("approvable");
        ExcludedField.addExcludedField("boundEntityId");
        ExcludedField.addExcludedField("boundEntityName");
        ExcludedField.addExcludedField("boundEntityNameLowerCase");
        ExcludedField.addExcludedField("boundEntityTaskLink");
        ExcludedField.addExcludedField("fileRevisionsForApprovalNote");
        ExcludedField.addExcludedField("1");
        ExcludedField.addExcludedField("startup");
        ExcludedField.addExcludedField("totalActivitiesDuration");
        ExcludedField.addExcludedField("confirmed");
        ExcludedField.addExcludedField("deadLine");
        ExcludedField.addExcludedField("approvalNotes");
        ExcludedField.addExcludedField("ApprovalNotes");
        ExcludedField.addExcludedField("boundEntityTaskLinks");
        ExcludedField.addExcludedField("boundTasks");
        ExcludedField.addExcludedField("extraLine");
        ExcludedField.addExcludedField("nbTaskOk");
        ExcludedField.addExcludedField("taskActivities");
        ExcludedField.addExcludedField("taskChanges");
        ExcludedField.addExcludedField("taskGroup");
        ExcludedField.addExcludedField("tasksActivitiesDuration");
        ExcludedField.addExcludedField("tasksDurationGap");
        ExcludedField.addExcludedField("tasksEstimation");
        ExcludedField.addExcludedField("tasksStatus");
        ExcludedField.addExcludedField("tasksWorkers");
        // ExcludedField.addExcludedField("version");
        ExcludedField.addExcludedField("activitiesDuration");
        ExcludedField.addExcludedField("boundEntity");
        ExcludedField.addExcludedField("internal uuid");
        ExcludedField.addExcludedField("commentForApprovalNote");
        ExcludedField.addExcludedField("internalUUID");
        ExcludedField.addExcludedField("updatedByApprovalNote");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TaskDurationCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskCANCELLEDCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskCLOSEDCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskOKCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskSTANDBYCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskWAITAPPCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskWIPCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TotalNbOfTasksCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TaskWorkersCollectionQuery");
        ExcludedField
                .addExcludedField("fr.hd3d.services.resources.collectionquery.TaskBoundEntityParentIdCollectionQuery");
    }

}
