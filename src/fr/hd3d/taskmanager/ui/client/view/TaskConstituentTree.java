package fr.hd3d.taskmanager.ui.client.view;

import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.widget.workobjecttree.ConstituentTree;
import fr.hd3d.taskmanager.ui.client.events.TaskManagerEvents;


/**
 * Constituent tree that allows task creation for selected constituent via a context menu.
 * 
 * @author HD3D
 */
public class TaskConstituentTree extends ConstituentTree
{
    public TaskConstituentTree(String rootName)
    {
        super(rootName);
    }

    @Override
    protected void setContextMenu()
    {
        super.setContextMenu(new CreateTaskMenu(this, TaskManagerEvents.CREATE_NEW_TASK_CONSTITUENT_CLICKED,
                ConstituentModelData.SIMPLE_CLASS_NAME));
    }
}
