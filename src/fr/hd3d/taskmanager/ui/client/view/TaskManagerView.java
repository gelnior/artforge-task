package fr.hd3d.taskmanager.ui.client.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.core.DomQuery;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.Viewport;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.RootPanel;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.ProjectAllCombobox;
import fr.hd3d.common.ui.client.widget.SavingStatus;
import fr.hd3d.common.ui.client.widget.SheetCombobox;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.constraint.widget.SheetFilterComboBox;
import fr.hd3d.common.ui.client.widget.dialog.CommentTaskDialog;
import fr.hd3d.common.ui.client.widget.dialog.ConfirmationDisplayer;
import fr.hd3d.common.ui.client.widget.explorer.ExplorerPanel;
import fr.hd3d.common.ui.client.widget.explorer.ExplorerRefreshButton;
import fr.hd3d.common.ui.client.widget.explorer.controller.ExplorerController;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerGrid;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.AutoSaveButton;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.ConstraintPanelToggle;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.DeleteSheetToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.EditSheetToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.NewSheetToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.PrintButton;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.SaveSheetDataToolItem;
import fr.hd3d.common.ui.client.widget.mainview.MainView;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.tasktype.TaskTypeEditor;
import fr.hd3d.taskmanager.ui.client.controller.TaskManagerController;
import fr.hd3d.taskmanager.ui.client.events.TaskManagerEvents;
import fr.hd3d.taskmanager.ui.client.model.TaskManagerModel;
import fr.hd3d.taskmanager.ui.client.view.column.TaskEditorProvider;
import fr.hd3d.taskmanager.ui.client.view.column.TaskManagerColumnInfoProvider;
import fr.hd3d.taskmanager.ui.client.view.listeners.ApprovalListener;


/**
 * Manages Task Manager Application main widgets.
 * 
 * @author HD3D
 */
public class TaskManagerView extends MainView
{

    /** Model that handles task manager data. */
    private final TaskManagerModel model = new TaskManagerModel();
    /** Controller that handles task . */
    private final TaskManagerController controller = new TaskManagerController(this, model);

    /** Panel containing all widgets. */
    private final BorderedPanel mainPanel = new BorderedPanel();
    /** Constituent navigation tree. */
    private final TaskConstituentTree constituentTree = new TaskConstituentTree("All");
    /** Shot navigation tree. */
    private final TaskShotTree shotTree = new TaskShotTree("All");

    /** Explorer displaying task data. */
    private ExplorerPanel explorer;
    /** Saving status indicator. */
    private final SavingStatus savingStatus = new SavingStatus();

    /** Project combo box allowing to select all projects. */
    private final ProjectAllCombobox projectComboBox = new ProjectAllCombobox();
    /** Combo box allowing to select sheet. */
    private final SheetCombobox sheetCombo = new SheetCombobox();
    /** Filter combobox needed to select filter to apply on current sheet. */
    protected SheetFilterComboBox sheetFilterComboBox = new SheetFilterComboBox();

    private final AutoSaveButton autoSaveButton = new AutoSaveButton();
    private final SaveSheetDataToolItem saveButton = new SaveSheetDataToolItem();
    private final DeleteSheetToolItem deleteViewButton = new DeleteSheetToolItem();
    private final NewSheetToolItem newViewButton = new NewSheetToolItem();
    private final EditSheetToolItem editViewButton = new EditSheetToolItem();
    private final ToolBarButton deleteRowButton = new ToolBarButton(Hd3dImages.getDeleteIcon(), CONSTANTS.DeleteRow(),
            TaskManagerEvents.DELETE_TASK_CLICKED);
    private final ToolBarButton odsButton = new ToolBarButton(Hd3dImages.getCsvExportIcon(), "Export all tasks to ods",
            null);

    /** Double click listener needed to expand and collapse displayed approval lists. */
    private ApprovalListener clicListener;
    /** Task type editor. */
    private TaskTypeEditor taskTypeEditor;

    private CommentTaskDialog commentTaskDialog;

    /** Default constructor, sets layout and set the controller. */
    public TaskManagerView()
    {
        super("Task Manager");
        EventDispatcher.get().addController(controller);

        this.model.setSheetStore(sheetCombo.getServiceStore());
        this.sheetCombo.addAvailableEntity(TaskModelData.SIMPLE_CLASS_NAME);
    }

    /** Build widgets and set listeners on them. */
    public void initTaskManagerView()
    {
        this.init();
        this.setListeners();
    }

    /**
     * Initialize all widgets and add them to the viewport.
     */
    @Override
    public void init()
    {
        commentTaskDialog = new CommentTaskDialog(ExplorerEvents.RELOAD_DATA,
                TaskManagerEvents.UPDATE_CELL_STATUS_CONFIRMED);
        this.createWestPanel();
        this.setExplorer();
        mainPanel.setTopComponent(createToolBar());

        Viewport viewport = new Viewport();
        viewport.setLayout(new FitLayout());
        viewport.add(mainPanel);

        RootPanel.get().add(viewport);

        this.taskTypeEditor = new TaskTypeEditor();
    }

    /**
     * @return Filter used by the explorer to filter data it retrieves.
     */
    public LogicConstraint getExplorerFilter()
    {
        return this.explorer.getFilter();
    }

    /**
     * Load from services open projects data in project combo box.
     */
    public void loadProjectComboBoxData()
    {
        this.projectComboBox.setData();
    }

    /**
     * Rebuilds explorer filter depending on application filter and user filter.
     */
    public void refreshExplorerFilter()
    {
        this.explorer.refreshFilter();
    }

    /**
     * Displays a confirmation dialog that ask for deletion confirmation.
     */
    public void askForDeleteConfirmation()
    {
        ConfirmationDisplayer.display(CONSTANTS.Confirm(), "Do you want to delete selected task(s)?",
                TaskManagerEvents.DELETE_TASK_CONFIRMED);
    }

    /**
     * Enable the sheet combo box.
     * 
     * @param enabled
     *            True if combo box is enabled.
     */
    public void enableSheetComboBox(boolean enabled)
    {
        this.sheetCombo.setEnabled(enabled);
    }

    /**
     * Reload sheets available in sheet combo box for given project.
     * 
     * @param project
     *            The project of which sheet will be loaded.
     */
    public void reloadSheets(ProjectModelData project)
    {
        this.sheetCombo.setProject(project);
        this.sheetCombo.reload();
    }

    /**
     * Configure explorer for given sheet (and reloads explorer data).
     * 
     * @param sheet
     *            The sheet to display in explorer.
     * @param isLoadData
     *            True to load data after setting sheet.
     */
    public void setExplorerSheet(SheetModelData sheet, boolean isLoadData)
    {
        this.explorer.setCurrentSheet(sheet, isLoadData);

        this.sheetFilterComboBox.refreshFilterList(sheet);
    }

    /**
     * Select the first sheet of the combo box.
     */
    public void selectFirstSheet()
    {
        if (sheetCombo.getStore().getCount() > 0)
        {
            this.sheetCombo.setValue(this.sheetCombo.getStore().getAt(0));
        }
        else
        {
            this.sheetCombo.setValue(null);
        }
    }

    /**
     * Select given sheet in sheet combo box.
     * 
     * @param sheet
     *            The sheet to select.
     */
    public void selectSheet(SheetModelData sheet)
    {
        if (sheet != null)
        {
            this.sheetCombo.setValue(sheet);
        }
    }

    /**
     * @return Currently selected sheet in combo box.
     */
    public SheetModelData getSelectedSheet()
    {
        return this.sheetCombo.getValue();
    }

    /**
     * Remove given sheet from combo box.
     * 
     * @param sheet
     *            The sheet to remove.
     */
    public void removeSheetFromCombo(SheetModelData sheet)
    {
        this.sheetCombo.getStore().remove(sheet);
    }

    /**
     * Reload tree data (constituent and shot tree) for given project.
     * 
     * @param project
     *            The project of which data will be loaded.
     */
    public void reloadTrees(ProjectModelData project)
    {
        this.constituentTree.setCurrentProject(project);
        this.shotTree.setCurrentProject(project);
    }

    /**
     * @return Tasks currently selected in explorer.
     */
    public List<Hd3dModelData> getSelectedTasks()
    {
        return this.explorer.getSelectedModels();
    }

    /**
     * Reloads explorer data.
     */
    public void reloadExplorerData()
    {
        this.explorer.refreshData();
    }

    /**
     * Shows saving indicator.
     */
    public void showSaving()
    {
        this.savingStatus.show();
    }

    /**
     * Hides saving indicator.
     */
    public void hideSaving()
    {
        this.savingStatus.hide();
    }

    /**
     * @return Selected constituent inside constituent tree. If none is selected, null is returned.
     */
    public List<RecordModelData> getSelectedConstituents()
    {
        return constituentTree.getSelection();
    }

    /**
     * @return Selected shot inside shot tree. If none is selected, null is returned.
     */
    public List<RecordModelData> getSelectedShots()
    {
        return shotTree.getSelection();
    }

    /**
     * Displays task creation dialog for given work object.
     * 
     * @param workObject
     *            The work object on which task will be created.
     */
    public void displayTaskCreator(RecordModelData workObject)
    {
        TaskCreatorDisplayer.show(workObject);
    }

    /**
     * Displays task creation dialog for given work objects.
     * 
     * @param workObjects
     *            work objects on which task will be created.
     */
    public void displayTaskCreator(List<RecordModelData> workObjects)
    {
        TaskCreatorDisplayer.show(workObjects);
    }

    /**
     * Creates assignment context menu and fill it with <i>personList</i>.
     * 
     * @param personList
     * @return Created context menu.
     */
    public Menu createMenuPersons(List<PersonModelData> personList)
    {
        Menu menu = new Menu();
        MenuItem menuItem = null;

        for (PersonModelData person : personList)
        {
            final Long person_id = person.getId();
            String stringValue = person.getFirstName() + " " + person.getLastName();

            menuItem = new MenuItem(stringValue);
            menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    AppEvent event = new AppEvent(TaskManagerEvents.UPDATE_CELL_WORKER, person_id);
                    EventDispatcher.forwardEvent(event);
                }
            });
            menu.add(menuItem);
        }
        return menu;
    }

    /**
     * Enables tree widgets.
     */
    public void enableTrees()
    {
        this.constituentTree.enable();
        this.shotTree.enable();
    }

    /**
     * Disables tree widgets.
     */
    public void clearTrees()
    {
        this.constituentTree.getStore().removeAll();
        this.shotTree.getStore().removeAll();
        this.constituentTree.setEnabled(false);
        this.shotTree.setEnabled(false);
    }

    /**
     * Displays task type editor (CRUD operations on task types).
     */
    public void displayTaskTypeEditor()
    {
        this.taskTypeEditor.show();
    }

    /**
     * @return Task type editor controller.
     */
    public Controller getTaskTypeEditorController()
    {
        return taskTypeEditor.getController();
    }

    /**
     * Enables delete button.
     */
    public void enableDeleteButton()
    {
        this.deleteRowButton.enable();
    }

    /**
     * Disables delete button.
     */
    public void disableDeleteButton()
    {
        this.deleteRowButton.disable();
    }

    /**
     * Set cookie key for retrieving last selected sheet ID.
     * 
     * @param cookieKey
     *            The cookie key to set.
     */
    public void setSheetCookie(String cookieKey)
    {
        this.sheetCombo.setCookieKey(cookieKey);
    }

    /**
     * Adds <i>parameter</i> to explorer data store.
     * 
     * @param parameter
     *            The parameter to add.
     */
    public void addParameterToExplorer(IUrlParameter parameter)
    {
        this.explorer.addParameter(parameter);
    }

    /**
     * Removes <i>parameter</i> to explorer data store.
     * 
     * @param parameter
     *            The parameter to remove.
     */
    public void removeParameterFromExplorer(IUrlParameter parameter)
    {
        this.explorer.removeParameter(parameter);
    }

    /**
     * Press auto save toggle button if it is not pressed.
     */
    public void toggleAutoSave()
    {
        this.autoSaveButton.toggle();
    }

    /**
     * Clear sheet combo filter (its store and selected value).
     */
    public void clearSheetFilter()
    {
        this.sheetFilterComboBox.setValue(null);
        this.sheetFilterComboBox.getStore().removeAll();
    }

    /**
     * Sets double click listener that notify controller when an approval cell is double clicked.<br>
     * Sets after edit listener. When task is modified, if its end date si null, it is updated by adding duration to
     * start date (week-ends are skipped).
     */
    public void setExplorerListener()
    {
        ExplorerGrid grid = this.explorer.getGrid();
        this.explorer.getGrid().addListener(Events.AfterEdit, new Listener<GridEvent<Hd3dModelData>>() {
            public void handleEvent(GridEvent<Hd3dModelData> be)
            {
                onTaskEdited();
            }
        });
        if (grid != null && !this.explorer.getGrid().getListeners(Events.CellDoubleClick).contains(this.clicListener))
        {
            if (this.clicListener == null)
                this.clicListener = new ApprovalListener(explorer.getGrid());
            this.explorer.getGrid().addListener(Events.CellDoubleClick, clicListener);
        }
    }

    /**
     * When task is modified, if its end date si null, it is updated by adding duration to start date (week-ends are
     * skipped).
     */
    private void onTaskEdited()
    {
        for (Hd3dModelData model : explorer.getSelectedModels())
        {
            Record rec = explorer.getStore().getRecord(model);
            if (model.get(TaskModelData.START_DATE_FIELD) != null && model.get(TaskModelData.DURATION_FIELD) != null)
            {
                Long duration = model.get(TaskModelData.DURATION_FIELD);
                Date startDate = model.get(TaskModelData.START_DATE_FIELD);

                TaskModelData task = new TaskModelData();
                task.setDuration(duration);
                task.setStartDate(startDate);
                rec.set(TaskModelData.END_DATE_FIELD, task.calculateEndDate().asDate());
                rec.setDirty(true);

                if (ExplorerController.autoSave)
                    EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA);
            }
        }
    }

    /**
     * Displays note children (older notes for same task) for a given ID. If notes child are already displayed, they are
     * hidden.
     * 
     * @param id
     *            ID of the child to hide.
     */
    public void hideOrDisplayApprovalChild(Long id)
    {
        NodeList<com.google.gwt.user.client.Element> elements = DomQuery.select("div.approval-childof-" + id);
        for (int i = 0; i < elements.getLength(); i++)
        {
            Element element = elements.getItem(i);

            if (!element.getClassName().contains("hidden"))
                element.addClassName("hidden");
            else
                element.removeClassName("hidden");
        }
    }

    /**
     * Set selection changed listener on project and sheet combo box.
     */
    private void setListeners()
    {
        this.projectComboBox.addSelectionChangedListener(new EventSelectionChangedListener<ProjectModelData>(
                TaskManagerEvents.PROJECT_CHANGED));
        this.sheetCombo.addSelectionChangedListener(new EventSelectionChangedListener<SheetModelData>(
                TaskManagerEvents.SHEET_CHANGED));
    }

    /**
     * Creates and set icons on task manager toolbar.
     * 
     * @return Created toolbar.
     */
    private ToolBar createToolBar()
    {
        ToolBar toolBar = new ToolBar();

        ConstraintPanelToggle filterPanelButton = new ConstraintPanelToggle();
        sheetFilterComboBox.registerToggleButton(filterPanelButton);
        CSSUtils.set1pxBorder(filterPanelButton, "transparent");

        toolBar.add(projectComboBox);
        toolBar.add(new SeparatorToolItem());
        toolBar.add(sheetCombo);
        toolBar.add(filterPanelButton);
        toolBar.add(sheetFilterComboBox);
        toolBar.add(new SeparatorToolItem());
        toolBar.add(deleteRowButton);
        // toolBar.add(new SeparatorToolItem());
        // toolBar.add(new TaskTypeToolItem());
        toolBar.add(new SeparatorToolItem());
        // toolBar.add(new CsvSheetExportToolItem());
        // toolBar.add(new LabelToolItem(" Raw data : "));
        toolBar.add(odsButton);
        toolBar.add(new PrintButton());

        toolBar.add(new SeparatorToolItem());
        toolBar.add(new ExplorerRefreshButton(this.explorer));
        toolBar.add(autoSaveButton);
        toolBar.add(saveButton);
        toolBar.add(savingStatus);
        // toolBar.add(new SeparatorToolItem());

        toolBar.add(new FillToolItem());
        toolBar.add(newViewButton);
        toolBar.add(editViewButton);
        toolBar.add(deleteViewButton);

        AppEvent eventDetail = new AppEvent(TaskManagerEvents.ODS_EXPORT_CLICKED, "Detail");
        odsButton.addSelectionListener(new ButtonClickListener(eventDetail));

        toolBar.setStyleAttribute("padding", "5px");
        this.projectComboBox.setWidth(200);

        return toolBar;
    }

    /**
     * Create navigation trees.
     */
    private void createWestPanel()
    {
        BorderedPanel treePanel = new BorderedPanel();
        treePanel.addNorth(constituentTree, 300, 0, 0, 0, 0);
        treePanel.addCenter(shotTree, 5, 0, 0, 0);

        mainPanel.addWest(treePanel, 200);
    }

    /**
     * Build explorer widget and configures it.
     */
    private void setExplorer()
    {
        List<EntityModelData> entities = new ArrayList<EntityModelData>();
        EntityModelData entity = new EntityModelData();
        String entityName = TaskModelData.SIMPLE_CLASS_NAME;
        entity.setName(ServicesField.getHumanName(entityName));
        entity.setClassName(entityName);
        entities.add(entity);

        this.explorer = new ExplorerPanel();
        this.explorer.setColumnInfoProvider(new TaskManagerColumnInfoProvider());
        this.explorer.setConstraintEditorProvider(new TaskEditorProvider());
        this.explorer.registerFilterComboBox(sheetFilterComboBox);
        this.explorer.setBorders(true);
        this.explorer
                .setDefaultOrderColumn("fr.hd3d.services.resources.collectionquery.TaskBoundEntityCollectionQuery");

        EventDispatcher.get().addController(this.explorer.getController());

        this.model.setExplorerStore(explorer.getStore());
        this.model.setEntities(entities);
        this.explorer.setApplicationFilter(this.model.getFilter());

        this.mainPanel.addCenter(explorer);

    }

    public void openCommentTaskDialog()
    {
        commentTaskDialog.setTask(getSelectedTasks());
        commentTaskDialog.show();
    }

    /**
     * Deselect all in shot tree.
     */
    public void clearShotSelection()
    {
        this.shotTree.clearSelection();
    }

    /**
     * Deselect all in constituent tree.
     */
    public void clearConstituentSelection()
    {
        this.constituentTree.clearSelection();
    }

    /**
     * Disable navigation trees
     */
	public void disableTrees() 
	{
		this.constituentTree.disable();
		this.shotTree.disable();
	}
}
