package fr.hd3d.taskmanager.ui.client.view;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;

import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.basetree.BaseTree;


/**
 * Menu that forwards create task clicked event to controllers for a tree selection.
 * 
 * @author HD3D
 */
public class CreateTaskMenu extends EasyMenu
{

    private static final String createNewTaskString = "Create new Task";
    private static final String refreshString = "Refresh";
    private static final String onlyOnShotConstituentString = "Only on Shot/Constituent";

    /** Class name of elements firing the event. Useful to not display add button on non task elements. */
    private final String simpleClassName;
    /** Create task item forwards the create task clicked event */
    private final MenuItem createTaskItem;

    private final MenuItem refreshItem;
    /** Tree on which menu is set. */
    private final BaseTree<?, ?> tree;

    /**
     * Constructor : configures tree menu item.
     * 
     * @param tree
     *            The tree on which menu is set.
     * @param event
     *            The event to forward.
     * @param simpleClassName
     *            Class name of elements firing the event. Useful to not display add button on non task elements.
     */
    public CreateTaskMenu(final BaseTree<?, ?> tree, EventType event, String simpleClassName)
    {
        this.tree = tree;
        this.simpleClassName = simpleClassName;
        this.createTaskItem = this.makeNewItem(createNewTaskString, event, Hd3dImages.getAddIcon());
        SelectionListener<MenuEvent> refreshListener = new SelectionListener<MenuEvent>() {

            @Override
            public void componentSelected(MenuEvent ce)
            {
                tree.refreshSelection();
            }

        };
        this.refreshItem = this.makeNewItem(refreshString, refreshListener, Hd3dImages.getRefreshIcon());
        this.add(createTaskItem);
        this.add(refreshItem);
    }

    /**
     * Display create task item only if selection corresponds to set class name.
     */
    @Override
    protected void onBeforeShow(BaseEvent event)
    {
        RecordModelData record = (RecordModelData) CollectionUtils.getFirst(this.tree.getSelection());
        if (record != null && simpleClassName.equals(record.getSimpleClassName()))
        {
            createTaskItem.setTitle("");
            createTaskItem.enable();
        }
        else
        {
            if (this.getItemCount() > 1)
            {
                createTaskItem.setTitle(onlyOnShotConstituentString);
                createTaskItem.disable();
            }
        }

    }
}
