package fr.hd3d.taskmanager.ui.client.view;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.TaskTypeReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;


public class TaskTypeComboBox extends ModelDataComboBox<TaskTypeModelData>
{

    public TaskTypeComboBox()
    {
        super(new TaskTypeReader());

        this.getListView().setTemplate(getXTemplate());
    }

    /**
     * Set combo styles.
     */
    @Override
    protected void setStyles()
    {
        super.setStyles();
        this.setEmptyText("Select Task Type...");
        this.setDisplayField(RecordModelData.NAME_FIELD);
        this.setTemplate(getXTemplate());
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
		return [
				'<tpl for=".">',
				'<div class="x-combo-list-item"> <span style="background-color:{color}; font-size: 13px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;<span style="font-weight: bold; font-size : 12px;">&nbsp;{name}&nbsp;&nbsp;</span> </div>',
				'</tpl>' ].join("");
    }-*/;
}
