package fr.hd3d.taskmanager.ui.client.view;

import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.widget.workobjecttree.ShotTree;
import fr.hd3d.taskmanager.ui.client.events.TaskManagerEvents;


/**
 * The task shot tree is a shot tree with a context menu allowing to create task for selected shots.
 * 
 * @author HD3D
 */
public class TaskShotTree extends ShotTree
{

    public TaskShotTree(String rootName)
    {
        super(rootName);
    }

    @Override
    protected void setContextMenu()
    {
        super.setContextMenu(new CreateTaskMenu(this, TaskManagerEvents.CREATE_NEW_TASK_SHOT_CLICKED,
                ShotModelData.SIMPLE_CLASS_NAME));
    }
}
