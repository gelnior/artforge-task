package fr.hd3d.taskmanager.ui.client.view;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Hidden;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.taskmanager.ui.client.controller.TaskManagerController;
import fr.hd3d.taskmanager.ui.client.events.TaskManagerEvents;
import fr.hd3d.taskmanager.ui.client.model.TaskManagerModel;


public class ExportOdsController extends Controller
{

    private TaskManagerModel model;
    private TaskManagerView view;
    private String jsonToExport;

    public ExportOdsController(TaskManagerModel model, TaskManagerView view)
    {
        super();
        this.model = model;
        this.view = view;
        registerEvents();
    }

    private void registerEvents()
    {
        this.registerEventTypes(TaskManagerEvents.ODS_EXPORT_CLICKED);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        if (TaskManagerEvents.ODS_EXPORT_CLICKED.equals(event.getType()))
        {
            String type = (String) event.getData();
            jsonToExport = "{" + createData(type) + "}";

            String request = "export-task-ods";
            Hidden hidden = new Hidden("json");
            hidden.setValue(jsonToExport);
            TaskManagerController.exportOdsFormPanel.setAction(RestRequestHandlerSingleton.getInstance().getServicesUrl()
                    + request);
            TaskManagerController.exportOdsFormPanel.clear();
            TaskManagerController.exportOdsFormPanel.add(hidden);
            TaskManagerController.exportOdsFormPanel.submit();
        }
    }

    private String createData(String type)
    {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("yyyy-MM-dd");
        String dateStart = dateTimeFormat.format(new DateWrapper(DatetimeUtil.today()).asDate());

        return "\"TaskRawData\":\"" + dateStart + "\", \"type\": \"" + type + "\", \"project-id\":"
                + this.model.getCurrentProject().getId();

    }

}
