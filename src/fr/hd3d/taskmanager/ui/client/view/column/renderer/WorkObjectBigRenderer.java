package fr.hd3d.taskmanager.ui.client.view.column.renderer;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridGroupRenderer;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;


/**
 * Render in a big font name of the entity linked to the current task.
 * 
 * @author HD3D
 * 
 * @param <M>
 */
public class WorkObjectBigRenderer<M extends ModelData> implements IExplorerCellRenderer<M>, GridGroupRenderer
{
    public String render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        return getWorkObjectHtml(obj);
    }

    public String render(GroupColumnData data)
    {
        return getWorkObjectHtml(data.gvalue);
    }

    /**
     * @param gValue
     * @return HTML representation of work object task name.
     */
    private String getWorkObjectHtml(Object gValue)
    {
        String stringValue = "";
        if (gValue != null && gValue instanceof JSONObject)
        {
            JSONObject workObject = (JSONObject) gValue;
            JSONValue name = workObject.get(ConstituentModelData.CONSTITUENT_LABEL);
            String nameString = name.isString().stringValue();
            stringValue = "<div style= 'padding: 3px; font-size: 16px; font-weight: bold;'>" + nameString + "</div>";
        }
        else
        {
            stringValue += "<i>Undefined</i>";
        }
        return stringValue;
    }
}
