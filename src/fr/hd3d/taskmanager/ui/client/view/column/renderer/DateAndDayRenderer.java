package fr.hd3d.taskmanager.ui.client.view.column.renderer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.google.gwt.i18n.client.DateTimeFormat;

import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;


public class DateAndDayRenderer<M extends ModelData> implements IExplorerCellRenderer<M>
{

    public static final String DATE_STRING = "yyyy-MM-dd (EEE)";
    public static DateTimeFormat DATE_F = DateTimeFormat.getFormat(DATE_STRING);

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);

        Object objStart = model.get("startDate");
        Object objEnd = model.get("endDate");

        String stringValue = "";
        if (obj != null)
        {
            if (obj instanceof Date && objStart != null && objEnd != null)
            {
                if (property.equals("endDate"))
                {
                    if (((Date) objStart).after((Date) objEnd))
                    {
                        stringValue = "<div style='width:100%; height:100%; text-align:left; background-color:"
                                + "#ff0000" + ";'>" + DATE_F.format((Date) objEnd) + " before start !</div>";
                        return stringValue;
                    }
                }
            }

            stringValue = DATE_F.format((Date) obj);
        }
        return stringValue;
    }

    public void handleGridEvent(GridEvent<M> ge)
    {

    }

    public List<EventType> getListenedEvent()
    {
        List<EventType> events = new ArrayList<EventType>();
        return events;
    }

}
