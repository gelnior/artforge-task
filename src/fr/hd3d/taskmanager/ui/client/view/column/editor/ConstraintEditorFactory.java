package fr.hd3d.taskmanager.ui.client.view.column.editor;

import com.extjs.gxt.ui.client.widget.form.Field;

import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.AutoMultiTaskTypeComboBox;
import fr.hd3d.common.ui.client.widget.AutoTaskTypeComboBox;
import fr.hd3d.common.ui.client.widget.DurationField;
import fr.hd3d.common.ui.client.widget.FieldComboBox;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;
import fr.hd3d.common.ui.client.widget.MultiNoteStatusComboBox;
import fr.hd3d.common.ui.client.widget.NoteStatusComboBox;


/**
 * Editor Factory provides specific cell editors for the constraint panel..
 * 
 * @author HD3D
 */
public class ConstraintEditorFactory
{
    /**
     * @return The task type editor (an auto-complete model data combobox).
     */
    public static ModelDataComboBox<TaskTypeModelData> getTaskTypeEditor()
    {
        return new AutoTaskTypeComboBox();
    }

    /**
     * @return The task type editor (an auto-complete model data combobox).
     */
    public static Field<?> getMultiTaskTypeEditor()
    {
        return new AutoMultiTaskTypeComboBox();
    }

    /**
     * @return The status editor : a note status combo box.
     */
    public static FieldComboBox getStatusEditor()
    {
        return new NoteStatusComboBox(true);
    }

    /**
     * @return Duration editor that converts number of day in number of milliseconds (unit understood by services).
     */
    public static DurationField getDurationEditor()
    {
        return new DurationField();
    }

    public static Field<?> getMultiStatusEditor()
    {
        return new MultiNoteStatusComboBox();
    }

}
