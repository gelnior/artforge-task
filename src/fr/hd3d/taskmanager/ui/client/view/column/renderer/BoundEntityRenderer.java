package fr.hd3d.taskmanager.ui.client.view.column.renderer;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridGroupRenderer;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;


public class BoundEntityRenderer<M extends ModelData> implements IExplorerCellRenderer<M>, GridGroupRenderer
{
    public String render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        return getBoundEntityRenderedValue(obj);
    }

    public String render(GroupColumnData data)
    {
        return getBoundEntityRenderedValue(data.gvalue);
    }

    private String getWorkObjectName(JSONObject workObject)
    {
        String stringValue = "";

        JSONValue classWo = workObject.get("class");

        if (classWo.isString().stringValue().equals(ShotModelData.CLASS_NAME))
        {
            JSONValue name = workObject.get("label");
            stringValue = name.isString().stringValue();
        }
        else if (classWo.isString().stringValue().equals(SequenceModelData.CLASS_NAME))
        {
            JSONValue name = workObject.get("name");
            JSONValue hook = workObject.get("hook");
            if (hook == null)
            {
                stringValue = name.isString().stringValue();
            }
            else
            {
                stringValue = name.isString().stringValue();
            }
        }
        else if (classWo.isString().stringValue().equals(ConstituentModelData.CLASS_NAME))
        {
            JSONValue name = workObject.get("label");
            stringValue = name.isString().stringValue();
        }
        else if (classWo.isString().stringValue().equals(CategoryModelData.CLASS_NAME))
        {
            JSONValue name = workObject.get("name");
            JSONValue hook = workObject.get("hook");
            if (hook == null)
            {
                stringValue = name.isString().stringValue();
            }
            else
            {
                stringValue = name.isString().stringValue();
            }
        }
        else
        {
            JSONValue name = workObject.get("name");
            if (name != null)
            {
                stringValue = name.isString().stringValue();
            }
            else
            {
                stringValue = "<i>Undefined</i>";
            }
        }

        return stringValue;
    }

    private String getBoundEntityRenderedValue(Object gValue)
    {
        String stringValue = "";
        if (gValue instanceof JSONObject)
        {
            JSONObject json = (JSONObject) gValue;
            stringValue = getWorkObjectName(json);
        }
        else
        {
            stringValue += "<i>Undefined</i>";
        }
        return stringValue;
    }

    public void HandleGridEvent(GridEvent<M> ge)
    {

    }

    public List<EventType> getListenedEvent()
    {
        List<EventType> events = new ArrayList<EventType>();

        return events;
    }

}
