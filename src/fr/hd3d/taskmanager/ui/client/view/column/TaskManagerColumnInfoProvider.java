package fr.hd3d.taskmanager.ui.client.view.column;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.widget.DurationField;
import fr.hd3d.common.ui.client.widget.NoteStatusComboBox;
import fr.hd3d.common.ui.client.widget.PersonComboBox;
import fr.hd3d.common.ui.client.widget.explorer.model.BaseColumnInfoProvider;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.editor.ExplorerCellEditor;
import fr.hd3d.common.ui.client.widget.grid.editor.FieldComboBoxEditor;
import fr.hd3d.common.ui.client.widget.grid.editor.ModelDataComboBoxEditor;
import fr.hd3d.taskmanager.ui.client.events.TaskManagerEvents;
import fr.hd3d.taskmanager.ui.client.view.column.renderer.ActivityHoursRenderer;
import fr.hd3d.taskmanager.ui.client.view.column.renderer.BoundEntityRenderer;
import fr.hd3d.taskmanager.ui.client.view.column.renderer.StatusNoteRenderer;
import fr.hd3d.taskmanager.ui.client.view.column.renderer.StatusRenderer;
import fr.hd3d.taskmanager.ui.client.view.column.renderer.TaskHoursRenderer;
import fr.hd3d.taskmanager.ui.client.view.column.renderer.TaskPersonRenderer;
import fr.hd3d.taskmanager.ui.client.view.column.renderer.TaskThumbnailRenderer;
import fr.hd3d.taskmanager.ui.client.view.column.renderer.TaskTypeRenderer;
import fr.hd3d.taskmanager.ui.client.view.column.renderer.WorkObjectBigRenderer;


/**
 * Provides renderers and editors specifically available for task manager. It returns right renderer or editor for a
 * given string.
 * 
 * @author HD3D
 */
public class TaskManagerColumnInfoProvider extends BaseColumnInfoProvider
{
    public TaskManagerColumnInfoProvider()
    {
        super();
    }

    @Override
    public GridCellRenderer<Hd3dModelData> getRenderer(String name)
    {
        if (name.equals(Renderer.DURATION))
        {
            return new TaskHoursRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.DURATION_ACTIVITY))
        {
            return new ActivityHoursRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.PERSON_TASK))
        {
            return new TaskPersonRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.TASK_STATUS))
        {
            return new StatusRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.WORK_OBJECT))
        {
            return new BoundEntityRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.WORK_OBJECT_BIG))
        {
            return new WorkObjectBigRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.TASK_TYPE))
        {
            return new TaskTypeRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.THUMBNAIL))
        {
            return new TaskThumbnailRenderer();
        }
        else if (name.equals(Renderer.APPROVAL))
        {
            return new StatusNoteRenderer();
        }

        return super.getRenderer(name);
    }

    @Override
    public CellEditor getEditor(String name, IColumn column)
    {
        CellEditor editor = super.getEditor(name, column);

        if (editor == null)
        {
            if (name.toLowerCase().equals(Editor.TASK_STATUS))
            {
                editor = new FieldComboBoxEditor(new NoteStatusComboBox());
            }
            else if (name.toLowerCase().equals(Editor.DURATION))
            {
                editor = new ExplorerCellEditor(new DurationField());
            }
            else if (PersonModelData.SIMPLE_CLASS_NAME.toLowerCase().equals(name.toLowerCase()))
            {
                PersonComboBox combo = new PersonComboBox();
                combo.setSelectionChangedEvent(TaskManagerEvents.PERSON_ASSIGNED);
                editor = new ModelDataComboBoxEditor<PersonModelData>(combo);
            }
        }

        return editor;
    }
}
