package fr.hd3d.taskmanager.ui.client.view.column.renderer;

import java.util.Iterator;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GridGroupRenderer;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.client.PersonNameUtils;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorateurColumnContextMenu;
import fr.hd3d.taskmanager.ui.client.config.TaskManagerConfig;
import fr.hd3d.taskmanager.ui.client.events.TaskManagerEvents;


public class TaskPersonRenderer<M extends ModelData> implements GridCellRenderer<M>, IExplorateurColumnContextMenu<M>,
        GridGroupRenderer
{
    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        return getPersonRenderedValue(obj, CSSUtils.CENTER);
    }

    public String render(GroupColumnData data)
    {
        return getPersonRenderedValue(data.gvalue, CSSUtils.LEFT);
    }

    private String getUserName(JSONObject user)
    {
        String stringValue = "";
        JSONValue firstName = user.get(PersonModelData.PERSON_FIRST_NAME_FIELD);
        JSONValue lastName = user.get(PersonModelData.PERSON_LAST_NAME_FIELD);
        JSONValue login = user.get(PersonModelData.PERSON_LOGIN_FIELD);
        // stringValue = firstName.isString().stringValue() + " " + lastName.isString().stringValue();
        stringValue = PersonNameUtils.getFullName(lastName.isString().stringValue(),
                firstName.isString().stringValue(), login.isString().stringValue());

        return stringValue;
    }

    private String getPersonRenderedValue(Object gValue, String alignment)
    {
        String stringValue = "";
        if (gValue instanceof JSONObject)
        {
            JSONObject json = (JSONObject) gValue;
            stringValue = getUserName(json);
        }
        else if (gValue instanceof List<?>)
        {
            @SuppressWarnings("unchecked")
            List<JSONValue> users = (List<JSONValue>) gValue;
            for (Iterator<JSONValue> iterator = users.iterator(); iterator.hasNext();)
            {
                Object value = iterator.next();
                stringValue += getPersonRenderedValue(value, CSSUtils.CENTER) + ",";
            }
            if (stringValue.length() > 0)
            {
                stringValue = stringValue.substring(0, stringValue.length() - 1);
            }
        }
        else if (gValue instanceof PersonModelData)
        {
            PersonModelData person = (PersonModelData) gValue;
            stringValue = PersonNameUtils.getFullName(person.getLastName(), person.getFirstName(), person.getLogin());
        }
        else
        {
            stringValue = "<div style='text-align:" + alignment + "; background-color:" + "#dcdcdc" + ";'>"
                    + "<i>Unassigned</i>" + "</div>";
        }
        return stringValue;
    }

    public void handleGridEvent(GridEvent<M> ge)
    {
        if (ge.getType() == Events.ContextMenu)
        {
            Menu menu = new Menu();

            menu.setAutoWidth(true);
            menu.setWidth(180);

            AppEvent event = new AppEvent(TaskManagerEvents.CREATE_MENU_ASSIGNATION);
            event.setData(TaskManagerConfig.PERSON_MENU_EVENT_VAR_NAME, menu);
            EventDispatcher.forwardEvent(event);

            ge.getGrid().setContextMenu(menu);
        }
    }
}
