package fr.hd3d.taskmanager.ui.client.view.column.renderer;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GridGroupRenderer;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;


public class TaskTypeRenderer<M extends ModelData> implements GridCellRenderer<M>, GridGroupRenderer
{
    public String render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        return getTaskTypeRenderedValue(obj, "center");
    }

    public String render(GroupColumnData data)
    {
        return getTaskTypeRenderedValue(data.gvalue, "left");
    }

    private String getTaskTypeRenderedValue(Object gValue, String alignment)
    {
        String stringValue = "";
        if (gValue instanceof JSONObject)
        {
            JSONObject json = (JSONObject) gValue;
            JSONValue nameJson = json.get("name");
            JSONValue colorJson = json.get("color");
            String color = "default color";

            if (colorJson != null)
            {
                color = colorJson.isString().stringValue();
            }

            String name = nameJson.isString().stringValue();

            if (!color.equals("default color"))
            {
                stringValue = "<div style='white-space: normal; padding: 3px; font-weight: bold ; text-align:"
                        + alignment + "; background-color:" + color + ";'>" + name + "</div>";
            }
            else
            {
                stringValue = "<div style='white-space: normal; padding: 3px; font-weight: bold ; text-align:"
                        + alignment + ";'>" + name + "</div>";
            }
        }
        else
        {
            stringValue += "<i>unknow task type</i>";
        }
        return stringValue;

    }

    public void HandleGridEvent(GridEvent<M> ge)
    {

    }

    public List<EventType> getListenedEvent()
    {
        List<EventType> events = new ArrayList<EventType>();

        return events;
    }

}
