package fr.hd3d.taskmanager.ui.client.view.column.renderer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalComparator;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskStatusMap;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.HtmlUtils;
import fr.hd3d.common.ui.client.widget.NoteStatusComboBox;


/**
 * Displays approvals as an expandable colored list : first approval is always displayed and others are displayed when
 * user double clicked on cell. Double clicked event is handled in other class (see double click listener on explorer).
 * 
 * @author HD3D
 */
public class StatusNoteRenderer implements GridCellRenderer<Hd3dModelData>
{
    private static final String APPROVAL_TEXT_CLASS = "approval-text";
    private static final String TASK_APPROVAL_DATE_CLASS = "task-approval-date";

    /** Comparator needed to sort approvals by date. */
    private final ApprovalComparator comparator = new ApprovalComparator();

    /**
     * Return HTML representation of note list.
     */
    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {
        List<ApprovalNoteModelData> notes = model.get(property);

        if (CollectionUtils.isNotEmpty(notes) && notes instanceof List<?>
                && notes.get(0) instanceof ApprovalNoteModelData)
        {
            Collections.sort(notes, comparator);
            return getNotesHtml(notes);
        }
        else
        {
            return "&nbsp;";
        }
    }

    /**
     * @param approvals
     *            List of approvals to render.
     * @return The HTML representation of approval list.
     */
    private String getNotesHtml(List<ApprovalNoteModelData> approvals)
    {
        FastMap<List<ApprovalNoteModelData>> noteLists = getTypedNoteList(approvals);
        String htmlRender = "";
        boolean isFirst = true;
        ApprovalNoteModelData firstNote = approvals.get(0);

        Collection<List<ApprovalNoteModelData>> approvalListsValues = noteLists.values();
        for (List<ApprovalNoteModelData> approvalList : approvalListsValues)
        {
            if (approvalListsValues.size() > 1)
            {
                ApprovalNoteModelData listFirstNote = approvalList.get(0);
                htmlRender += getFirstNoteTitleHtml(listFirstNote, firstNote);
            }

            for (ApprovalNoteModelData approval : approvalList)
            {
                Element approvalDiv = getNoteHtml(approval);

                if (!isFirst)
                {
                    approvalDiv.addClassName("approval-childof-" + firstNote.getId());
                    approvalDiv.addClassName("hidden");
                }
                else
                {
                    int retakeCount = 0;
                    for (ApprovalNoteModelData note : approvals)
                        if (ETaskStatus.RETAKE.toString().equals(note.getStatus()))
                            retakeCount++;
                    approvalDiv = getFirstNoteHtml(approval, retakeCount);
                }

                htmlRender += approvalDiv.getString();
                isFirst = false;
            }
        }

        return htmlRender;
    }

    /**
     * @param approvals
     *            Approvals to group.
     * @return Approvals grouped (list of list) by note type (one list by type).
     */
    private FastMap<List<ApprovalNoteModelData>> getTypedNoteList(List<ApprovalNoteModelData> approvals)
    {
        FastMap<List<ApprovalNoteModelData>> approvalLists = new FastMap<List<ApprovalNoteModelData>>();

        for (ApprovalNoteModelData approval : approvals)
        {
            String approvalKey = approval.getApprovalNoteType().toString();
            List<ApprovalNoteModelData> approvalList = approvalLists.get(approvalKey);
            if (approvalList == null)
            {
                approvalList = new ArrayList<ApprovalNoteModelData>();
                approvalLists.put(approvalKey, approvalList);
            }
            approvalList.add(approval);
        }

        return approvalLists;
    }

    /**
     * @param listFirstNote
     *            The first note of list of which title is required.
     * @param firstNote2
     *            The first note that is always displayed.
     * 
     * @return HTML representation of first note (note that is always displayed : when approval list is expanded or
     *         not).
     */
    private String getFirstNoteTitleHtml(ApprovalNoteModelData listFirstNote, ApprovalNoteModelData firstNote)
    {
        Element titleDiv = DOM.createDiv();
        titleDiv.setInnerHTML(listFirstNote.getApprovalTypeName());
        titleDiv.addClassName("approval-childof-" + firstNote.getId());
        titleDiv.addClassName("task-approval-title");
        titleDiv.addClassName(CSSUtils.HIDDEN_CLASS);
        return titleDiv.getString();
    }

    /**
     * @param note
     *            The note to render.
     * 
     * @return note colored HTML representation.
     */
    private Element getNoteHtml(ApprovalNoteModelData approval)
    {
        return getNoteHtml(approval.getDate(), approval.getComment(), TaskStatusMap.getColorForStatus(approval
                .getStatus()), NoteStatusComboBox.getStatusAbbreviationMap().get(approval.getStatus()));
    }

    /**
     * Render note corresponding to given parameters.
     * 
     * @param date
     *            The note date.
     * @param text
     *            The note text.
     * @param color
     *            The note color.
     * @param status
     *            The note status.
     * 
     * @return note colored HTML representation.
     */
    protected Element getNoteHtml(Date date, String text, String color, String status)
    {
        String stringDate = DateFormat.FRENCH_DATE_TIME.format(date);
        Element html = getNoteDiv(text, stringDate, color);
        Element myDate = DOM.createSpan();

        myDate.addClassName(TASK_APPROVAL_DATE_CLASS);
        myDate.setInnerHTML(stringDate);
        html.appendChild(myDate);

        if (text != null)
        {
            Element div = DOM.createDiv();
            div.addClassName(APPROVAL_TEXT_CLASS);
            text = HtmlUtils.changeToHyperText(text);
            div.setInnerHTML(text.replace("\n", "<br />"));
            html.appendChild(div);
        }

        return html;
    }

    private Element getFirstNoteHtml(ApprovalNoteModelData approval, int retakeCount)
    {
        return getFistNoteHtml(approval.getDate(), approval.getComment(), TaskStatusMap.getColorForStatus(approval
                .getStatus()), NoteStatusComboBox.getStatusAbbreviationMap().get(approval.getStatus()), retakeCount);
    }

    protected Element getFistNoteHtml(Date date, String text, String color, String status, int retakeCount)
    {
        String stringDate = DateFormat.FRENCH_DATE_TIME.format(date);
        Element html = getNoteDiv(text, stringDate, color);
        Element myDate = DOM.createSpan();

        myDate.addClassName(TASK_APPROVAL_DATE_CLASS);
        if (retakeCount > 0)
            myDate.setInnerHTML(stringDate + " (" + retakeCount + ")");
        else
            myDate.setInnerHTML(stringDate);
        html.appendChild(myDate);

        if (text != null)
        {
            Element div = DOM.createDiv();
            div.addClassName(APPROVAL_TEXT_CLASS);
            text = HtmlUtils.changeToHyperText(text);
            div.setInnerHTML(text.replace("\n", "<br />"));
            html.appendChild(div);
        }

        return html;
    }

    /**
     * @param text
     *            Note text.
     * @param stringDate
     *            Note date.
     * @param color
     *            Note color.
     * 
     * @return DIV tag for note rendering (styles already applied).
     */
    protected Element getNoteDiv(String text, String stringDate, String color)
    {
        Element div = DOM.createDiv();
        div.setAttribute("style", "white-space:normal;padding:3px 3px 3px 5px;background-color:" + color + ";");
        return div;
    }
}
