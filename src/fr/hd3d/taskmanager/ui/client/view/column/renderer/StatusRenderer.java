package fr.hd3d.taskmanager.ui.client.view.column.renderer;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GridGroupRenderer;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskStatusMap;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorateurColumnContextMenu;
import fr.hd3d.taskmanager.ui.client.events.TaskManagerEvents;


public class StatusRenderer<M extends Hd3dModelData> implements GridCellRenderer<M>,
        IExplorateurColumnContextMenu<Hd3dModelData>, GridGroupRenderer
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    private final String[] statusListMenu = { COMMON_CONSTANTS.StandBy(), COMMON_CONSTANTS.WorkInProgress(),
            COMMON_CONSTANTS.WaitApproval(), COMMON_CONSTANTS.Ok(), COMMON_CONSTANTS.Cancelled(),
            COMMON_CONSTANTS.Closed(), COMMON_CONSTANTS.Todo(), COMMON_CONSTANTS.Retake() };

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        return getStatusRenderedValue(obj, CSSUtils.CENTER);
    }

    public String render(GroupColumnData data)
    {
        return getStatusRenderedValue(data.gvalue, CSSUtils.LEFT);
    }

    private String getStatusRenderedValue(Object gValue, String alignment)
    {
        String status = (String) gValue;
        String cellCode = "";
        if (!Util.isEmptyString(status))
        {
            cellCode = status;
            if (ETaskStatus.OK.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.OK, COMMON_CONSTANTS.Ok(), alignment);
            }
            else if (ETaskStatus.WAIT_APP.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.WAIT_APP, COMMON_CONSTANTS.WaitApproval(), alignment);
            }
            else if (ETaskStatus.CANCELLED.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.CANCELLED, COMMON_CONSTANTS.Cancelled(), alignment);
            }
            else if (ETaskStatus.STAND_BY.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.STAND_BY, COMMON_CONSTANTS.StandBy(), alignment);
            }
            else if (ETaskStatus.WORK_IN_PROGRESS.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.WORK_IN_PROGRESS, COMMON_CONSTANTS.WorkInProgress(), alignment);
            }
            else if (ETaskStatus.CLOSE.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.CLOSE, COMMON_CONSTANTS.Closed(), alignment);
            }
            else if (ETaskStatus.TO_DO.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.TO_DO, COMMON_CONSTANTS.Todo(), alignment);
            }
            else if (cellCode.equals(ETaskStatus.RETAKE.toString()))
            {
                cellCode = getStringValue(ETaskStatus.RETAKE, COMMON_CONSTANTS.Retake(), alignment);
            }
            else if (cellCode.equals(ETaskStatus.NEW.toString()))
            {
                cellCode = getStringValue(ETaskStatus.NEW, "-", alignment);
            }
        }
        else
        {
            cellCode += "unknow status";
        }
        return cellCode;
    }

    private String getStringValue(ETaskStatus status, String text, String alignment)
    {
        return "<div style=' padding: 3px;  white-space: normal; font-weight: bold ; text-align:" + alignment
                + "; background-color:" + TaskStatusMap.getColorForStatus(status.toString()) + ";'>" + text + "</div>";
    }

    private Menu createMenuStatus(final Grid<Hd3dModelData> grid)
    {
        Menu men = new Menu();
        men.setAutoWidth(true);
        int i = 0;

        MenuItem menuItem = null;
        for (final String statusName : statusListMenu)
        {
            menuItem = new MenuItem(statusName);
            final int ind = i;
            menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    AppEvent event = new AppEvent(TaskManagerEvents.UPDATE_CELL_STATUS);

                    event.setData((ETaskStatus.values())[ind].toString());

                    EventDispatcher.forwardEvent(event);
                }
            });
            men.add(menuItem);
            i += 1;
        }
        men.setWidth(180);
        return men;
    }

    public void handleGridEvent(GridEvent<Hd3dModelData> ge)
    {
        if (ge.getType() == Events.ContextMenu)
        {
            ge.getGrid().setContextMenu(this.createMenuStatus(ge.getGrid()));
        }
    }
}
