package fr.hd3d.taskmanager.ui.client.view.column.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.grid.renderer.DurationRenderer;


public class DurationRender<T> implements GridCellRenderer<Hd3dModelData>
{
    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {
        Long duration = model.get(property);
        return styledDuration(model, duration, DurationRenderer.getDurationString(duration));
    }

    protected String styledDuration(Hd3dModelData model, Long duration, String renderString)
    {
        return renderString;
    }
}
