package fr.hd3d.taskmanager.ui.client.view.column.renderer;

import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.ThumbnailRenderer;
import fr.hd3d.common.ui.client.widget.thumbnaileditor.PreviewDisplayer;
import fr.hd3d.common.ui.client.widget.thumbnaileditor.ThumbnailPath;


public class TaskThumbnailRenderer extends ThumbnailRenderer<Hd3dModelData>
{
    private String entity;
    private Long id;

    @Override
    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {
        String path = "";
        String value = model.get(property);
        if (!Util.isEmptyString(value))
        {
            String[] values = value.split("\\|");
            entity = values[0];
            String idString = values[1];
            try {
            	id = Long.parseLong(idString);
            
	            if (ConstituentModelData.SIMPLE_CLASS_NAME.equals(entity))
	            {
	                ConstituentModelData constituent = new ConstituentModelData();
	                constituent.setId(id);
	                path = ThumbnailPath.getPath(constituent);
	            }
	            else if (ShotModelData.SIMPLE_CLASS_NAME.equals(entity))
	            {
	                ShotModelData shot = new ShotModelData();
	                shot.setId(id);
	                path = ThumbnailPath.getPath(shot);
	            }
            } 
            catch (NumberFormatException e) 
            {
            	Logger.warn("Work object has no thumbnail linked to him.");
            }
        }
        return getImageHtmlCode(path);
    }

    @Override
    protected Menu createContextMenu(final Hd3dModelData model)
    {
        Menu menu = new Menu();
        menu.setAutoWidth(true);

        MenuItem menuItem = new MenuItem("Show Preview");
        menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                String path = "";
                if (FieldUtils.isConstituent(entity))
                {
                    ConstituentModelData constituent = new ConstituentModelData();
                    constituent.setId(id);
                    path = ThumbnailPath.getPath(constituent, true);
                }
                else if (FieldUtils.isShot(entity))
                {
                    ShotModelData shot = new ShotModelData();
                    shot.setId(id);
                    path = ThumbnailPath.getPath(shot, true);
                }

                PreviewDisplayer.displayPreview(path);
            }
        });

        menu.add(menuItem);

        return menu;
    }

    @Override
    public void handleGridEvent(GridEvent<Hd3dModelData> ge)
    {

    }
}
