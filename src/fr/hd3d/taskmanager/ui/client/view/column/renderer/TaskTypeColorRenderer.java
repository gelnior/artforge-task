package fr.hd3d.taskmanager.ui.client.view.column.renderer;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;

import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;


public class TaskTypeColorRenderer<M extends ModelData> implements IExplorerCellRenderer<M>
{
    public String render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        return getTaskTypeRenderedValue(obj);
    }

    public String render(GroupColumnData data)
    {
        return getTaskTypeRenderedValue(data.gvalue);
    }

    private String getTaskTypeRenderedValue(Object gValue)
    {
        String stringValue = "";

        if (gValue instanceof String)
        {

            String color = gValue.toString();

            stringValue = "<div style='width:100%; height:100%; text-align:center; background-color:" + color + ";'>"
                    + "." + "</div>";
        }

        return stringValue;
    }

    public void HandleGridEvent(GridEvent<M> ge)
    {

    }

    public List<EventType> getListenedEvent()
    {
        List<EventType> events = new ArrayList<EventType>();

        return events;
    }

}
