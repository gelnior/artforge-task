package fr.hd3d.taskmanager.ui.client.view.column;

import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.widget.form.Field;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.widget.FieldComboBox;
import fr.hd3d.common.ui.client.widget.constraint.IEditorProvider;
import fr.hd3d.taskmanager.ui.client.view.column.editor.ConstraintEditorFactory;


/**
 * Needed by Explorer to specify special column behaviors.
 * 
 * @author HD3D
 */
public class TaskEditorProvider implements IEditorProvider
{
    /**
     * Appropriate editor corresponding to the name given in argument.
     */
    public Field<?> getEditor(String name, EConstraintOperator operator, BaseModelData modelData)
    {
        if (name.equals(Editor.TASK_TYPE))
        {
            if (EConstraintOperator.in == operator)
                return ConstraintEditorFactory.getMultiTaskTypeEditor();
            else
                return ConstraintEditorFactory.getTaskTypeEditor();
        }
        else if (name.equals(Editor.TASK_STATUS))
        {
            if (EConstraintOperator.in == operator)
                return ConstraintEditorFactory.getMultiStatusEditor();
            else
                return ConstraintEditorFactory.getStatusEditor();
        }
        else if (name.equals(Editor.DURATION))
        {
            return ConstraintEditorFactory.getDurationEditor();
        }
        else if (name.equals(Editor.LIST)) 
        {
			List<String> listValues = modelData.get(ItemModelData.LIST_VALUES_FIELD);
			if (listValues != null) {
				FieldComboBox comboBox = new FieldComboBox();
				int index = 0;
				for (String listValue : listValues) {
					comboBox.addField(index++, listValue, listValue);
				}
				return comboBox;
			}
		}

        return null;
    }

    public String getItemConstraintColumnName(String name)
    {
        return null;
    }
}
