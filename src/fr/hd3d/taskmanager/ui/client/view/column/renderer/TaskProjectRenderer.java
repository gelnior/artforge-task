package fr.hd3d.taskmanager.ui.client.view.column.renderer;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;


public class TaskProjectRenderer<M extends ModelData> implements IExplorerCellRenderer<M>
{
    // private String separator = ", ";

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        return this.getValueFromObject(obj);
    }

    public void setSeparator(String separator)
    {
    // this.separatorS = separator;
    }

    public String render(GroupColumnData data)
    {
        return this.getValueFromObject(data.gvalue);
    }

    private String getValueFromObject(Object obj)
    {
        String stringValue = "";
        if (obj instanceof JSONObject)
        {
            JSONObject json = (JSONObject) obj;
            JSONValue name = json.get("name");
            stringValue = name.isString().stringValue();
        }
        else if (obj instanceof Long)
        {
            // Long value = new Long(((Long) obj).longValue());
            // ProjectModelData projectModelData = TaskManagerSession.taskManagerView.getProjectComboBox().getStore()
            // .findModel("id", value);
            //
            // if (projectModelData != null)
            // {
            //
            // stringValue = "<div style='width:100%; height:100%; text-align:center; background-color:"
            // + projectModelData.getColor() + ";'>" + projectModelData.getName() + "</div>";
            // }
        }
        else
        {
            stringValue = "All";
        }
        return stringValue;
    }

}
