package fr.hd3d.taskmanager.ui.client.view.column.renderer;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IAggregationRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.DurationRenderer;


/**
 * Displays activity duration in a human readable way. Moreover if acitvity duration is over estimated duration,
 * activities are displayed in a big shap and have red color.
 * 
 * @author HD3D
 * @param <T>
 *            Type of model to display.
 */
public class ActivityHoursRenderer<T extends Hd3dModelData> extends DurationRender<T> implements IAggregationRenderer
{
    @Override
    protected String styledDuration(Hd3dModelData model, Long duration, String renderString)
    {
        Long taskDuration = model.get(TaskModelData.DURATION_FIELD);
        String status = model.get(TaskModelData.STATUS_FIELD);

        if (taskDuration != null && duration != null && taskDuration < duration)
        {
            Element div = DOM.createDiv();
            div.addClassName("big-text");
            div.addClassName("red");
            div.setInnerHTML(renderString);
            renderString = div.getString();
        }
        else if (taskDuration != null && duration != null && taskDuration > duration && "OK".equals(status))
        {
            Element div = DOM.createDiv();
            // div.addClassName("big-text");
            div.addClassName("green");
            div.setInnerHTML(renderString);
            renderString = div.getString();
        }
        return renderString;
    }

    public Object renderAggregate(Object value)
    {
        Element div = DOM.createDiv();
        if (value != null && value instanceof Double)
            div.setInnerHTML(DurationRenderer.getDurationString(((Double) value).longValue()));
        return div.getString();
    }

}
