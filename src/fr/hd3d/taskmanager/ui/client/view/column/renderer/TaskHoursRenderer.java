package fr.hd3d.taskmanager.ui.client.view.column.renderer;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.menu.SeparatorMenuItem;
import com.google.gwt.user.client.Element;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IAggregationRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorateurColumnContextMenu;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.DurationRenderer;
import fr.hd3d.taskmanager.ui.client.events.TaskManagerEvents;


public class TaskHoursRenderer<M extends ModelData> implements IExplorerCellRenderer<M>,
        IExplorateurColumnContextMenu<M>, IAggregationRenderer
{

    private final String[] listDay = { "Days", "0.5", "1", "1.5", "2", "2.5", "3", "3.5", "4", "4.5", "5", "6", "7",
            "8", "9", "10", "15", "20" };

    private final Long[] listSecond = { 1L, 14400L, Long.valueOf(DatetimeUtil.DAY_SECONDS),
            Long.valueOf(DatetimeUtil.DAY_SECONDS + 14400), Long.valueOf(DatetimeUtil.DAY_SECONDS) * 2,
            Long.valueOf(DatetimeUtil.DAY_SECONDS * 2 + 14400), Long.valueOf(DatetimeUtil.DAY_SECONDS * 3),
            Long.valueOf(DatetimeUtil.DAY_SECONDS * 3 + 14400), Long.valueOf(DatetimeUtil.DAY_SECONDS * 4),
            Long.valueOf(DatetimeUtil.DAY_SECONDS * 4 + 14400), Long.valueOf(DatetimeUtil.DAY_SECONDS * 5),
            Long.valueOf(DatetimeUtil.DAY_SECONDS * 6), Long.valueOf(DatetimeUtil.DAY_SECONDS * 7),
            Long.valueOf(DatetimeUtil.DAY_SECONDS) * 8, Long.valueOf(DatetimeUtil.DAY_SECONDS * 9),
            Long.valueOf(DatetimeUtil.DAY_SECONDS * 10), Long.valueOf(DatetimeUtil.DAY_SECONDS) * 15,
            Long.valueOf(DatetimeUtil.DAY_SECONDS * 20) };

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);

        return DurationRenderer.getDurationString((Long) obj);
    }

    public String render(GroupColumnData data)
    {
        return DurationRenderer.getDurationString((Long) data.gvalue);
    }

    private Menu createMenu(final Grid<M> grid, Element element)
    {

        Menu men = new Menu();
        men.setAutoHeight(true);
        MenuItem menuItem = null;

        int index = 0;
        for (final String durationText : listDay)
        {
            final Long duration = listSecond[index];
            menuItem = new MenuItem(durationText);

            if (duration < 2.0)
            {
                menuItem.setText("<div style= 'font-weight: bold;'>" + durationText + "</div>");
                men.add(menuItem);
                men.add(new SeparatorMenuItem());
            }
            else
            {
                men.add(menuItem);
                menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
                    @Override
                    public void componentSelected(MenuEvent ce)
                    {
                        AppEvent event = new AppEvent(TaskManagerEvents.UPDATE_CELL_DURATION);
                        event.setData("GRID", grid);
                        event.setData("DURATION", duration);
                        EventDispatcher.forwardEvent(event);
                    }
                });
            }
            index++;
        }
        men.setWidth(75);
        men.setAutoWidth(true);
        men.setMaxHeight(600);

        men.hide();
        men.show(element, "c");

        return men;
    }

    public void handleGridEvent(GridEvent<M> ge)
    {
        if (ge.getType() == Events.ContextMenu)
        {
            ge.getGrid().setContextMenu(this.createMenu(ge.getGrid(), ge.getTarget()));
        }
    }

    public List<EventType> getListenedEvent()
    {
        List<EventType> events = new ArrayList<EventType>();
        events.add(Events.ContextMenu);
        return events;
    }

    public Object renderAggregate(Object value)
    {
        if (value instanceof Double)
            value = ((Double) value).longValue();
        return DurationRenderer.getDurationString((Long) value);
    }

}
