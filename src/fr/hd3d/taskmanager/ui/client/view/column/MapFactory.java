package fr.hd3d.taskmanager.ui.client.view.column;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;


/**
 * MapFactory provides map used for filling combo store which displays enumeration values list.
 * 
 * @author HD3D
 */
public class MapFactory
{
    /** Map which contains available values for task status. */
    private static FastMap<FieldModelData> statusMap;

    /** @return map which contains available values for task status. */
    public static FastMap<FieldModelData> getStatusMap()
    {
        if (statusMap == null)
        {
            statusMap = new FastMap<FieldModelData>();

            FieldUtils.addFieldToMap(statusMap, 0, "Stand by", ETaskStatus.STAND_BY.toString());
            FieldUtils.addFieldToMap(statusMap, 1, "Work in progress", ETaskStatus.WORK_IN_PROGRESS.toString());
            FieldUtils.addFieldToMap(statusMap, 2, "Wait approval", ETaskStatus.WAIT_APP.toString());
            FieldUtils.addFieldToMap(statusMap, 3, "OK", ETaskStatus.OK.toString());
            FieldUtils.addFieldToMap(statusMap, 4, "Cancelled", ETaskStatus.CANCELLED.toString());
            FieldUtils.addFieldToMap(statusMap, 5, "Closed", ETaskStatus.CLOSE.toString());
            FieldUtils.addFieldToMap(statusMap, 6, "Work in progress", ETaskStatus.TO_DO.toString());
        }

        return statusMap;
    }
}
