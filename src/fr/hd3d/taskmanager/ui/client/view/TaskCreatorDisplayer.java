package fr.hd3d.taskmanager.ui.client.view;

import java.util.List;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * Tool class to display task creator dialog.
 * 
 * @author HD3D
 */
public class TaskCreatorDisplayer
{
    /** Dialog needed for displaying task creation form. */
    private static TaskCreationDialog taskCreator;

    /** @return Dialog allowing to create task for a work object. */
    public static TaskCreationDialog getDialog()
    {
        if (taskCreator == null)
        {
            taskCreator = new TaskCreationDialog();
        }
        return taskCreator;
    }

    /**
     * Display task creator dialog for given work object.
     * 
     * @param workObject
     *            The work object on which task will be created.
     */
    public static void show(RecordModelData workObject)
    {
        getDialog().setWorkObject(workObject);
        getDialog().show();
    }

    /**
     * Display task creator dialog for given work objects.
     * 
     * @param workObjects
     *            The work objects on which tasks will be created.
     */
    public static void show(List<RecordModelData> workObjects)
    {
        getDialog().setWorkObject(workObjects);
        getDialog().show();
    }

}
