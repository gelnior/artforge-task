package fr.hd3d.taskmanager.ui.client.view;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.dialog.FormDialog;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.taskmanager.ui.client.events.TaskManagerEvents;


/**
 * Task creation dialog allows user to create task for a given task type.
 * 
 * @author HD3D
 */
public class TaskCreationDialog extends FormDialog
{
    /** Combobox used to select task type. */
    protected TaskTypeComboBox taskTypeComboBox = new TaskTypeComboBox();
    /** Work object on which task will be added. */
    protected ArrayList<RecordModelData> workObjects = new ArrayList<RecordModelData>();

    protected EqConstraint entityConstraint = new EqConstraint(TaskTypeModelData.ENTITY_NAME_FIELD);

    /**
     * Constructor : set styles and widgets.
     */
    public TaskCreationDialog()
    {
        super(TaskManagerEvents.TASK_CREATED, "");

        this.setWidth(330);
        this.taskTypeComboBox.setLabelSeparator(":");
        this.taskTypeComboBox.setFieldLabel("Task type");
        this.taskTypeComboBox.getServiceStore().addParameter(entityConstraint);
        this.panel.add(taskTypeComboBox);
    }

    @Override
    public void show()
    {
        this.taskTypeComboBox.getServiceStore().setPath(
                ServicesPath.PROJECTS + MainModel.getCurrentProject().getId() + "/" + ServicesPath.TASKTYPES);
        this.taskTypeComboBox.getServiceStore().reload();
        super.show();
    }

    /** Register work object to task creator. */
    public void setWorkObject(RecordModelData workObject)
    {
        this.workObjects.clear();
        this.workObjects.add(workObject);
        this.setHeading("Create Task for " + FieldUtils.getModelName(workObject));
    }

    /**
     * When OK button is clicked, a new task is created for current work object. When task save is finishes it makes the
     * entity task link between new task and current work object. Then it dispatches a task creation success event.
     * 
     * @throws Hd3dException
     */
    @Override
    protected void onOkClicked() throws Hd3dException
    {

        if (taskTypeComboBox.getValue() == null)
        {
            throw new Hd3dException("Please set a task type.");
        }

        for (RecordModelData workObject : workObjects)
        {
            TaskModelData.createForWorkObject(taskTypeComboBox.getValue(), workObject, MainModel.getCurrentUser(),
                    MainModel.getCurrentProject(), okEvent);
        }

    }

    public void setWorkObject(List<RecordModelData> workObjects)
    {
        this.workObjects.clear();
        this.workObjects.addAll(workObjects);
        StringBuilder header = new StringBuilder();
        header.append("Create Task for ");

        String entityName = null;
        for (RecordModelData workObject : workObjects)
        {
            if (entityName == null)
                entityName = workObject.getSimpleClassName();
            header.append(FieldUtils.getModelName(workObject)).append(", ");
        }
        entityConstraint.setLeftMember(entityName);
        header.delete(header.length() - 2, header.length());
        this.setHeading(header.toString());
    }
}
