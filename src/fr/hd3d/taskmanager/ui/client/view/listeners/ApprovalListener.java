package fr.hd3d.taskmanager.ui.client.view.listeners;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerGrid;
import fr.hd3d.taskmanager.ui.client.config.TaskManagerConfig;
import fr.hd3d.taskmanager.ui.client.events.TaskManagerEvents;
import fr.hd3d.taskmanager.ui.client.view.column.renderer.StatusNoteRenderer;


/**
 * Double click listener dedicated to explorer grid. Handle double click on validations and checkbox columns.
 * 
 * @author HD3D
 */
public class ApprovalListener implements Listener<BaseEvent>
{
    /** The explorer grid. */
    private ExplorerGrid grid;

    /**
     * Default constructor
     * 
     * @param grid
     *            Grid which has been double clicked.
     */
    public ApprovalListener(ExplorerGrid grid)
    {
        this.grid = grid;
    }

    /**
     * Set grid on which double click occur.
     * 
     * @param grid
     *            the grid to set.
     * */
    public void setGrid(ExplorerGrid grid)
    {
        this.grid = grid;
    }

    /**
     * Check if the column clicked is a validation column or a check box column.
     * 
     * @param be
     *            Double click event.
     */
    @SuppressWarnings("unchecked")
    public void handleEvent(BaseEvent be)
    {
        GridEvent<Hd3dModelData> ge = (GridEvent<Hd3dModelData>) be;

        ColumnConfig column = this.grid.getColumnModel().getColumn(ge.getColIndex());
        Object renderer = column.getRenderer();

        if (renderer instanceof StatusNoteRenderer)
        {
            this.onValidationCellDoubleClick(ge, column);
        }
    }

    /**
     * When validation column is clicked, it forwards correct event to controllers.
     * 
     * @param ge
     *            Grid event corresponding to double click.
     * @param column
     *            The column of which a cell was double clicked.
     */
    private void onValidationCellDoubleClick(GridEvent<Hd3dModelData> ge, ColumnConfig column)
    {
        AppEvent event = new AppEvent(TaskManagerEvents.APPROVAL_DOUBLE_CLICKED);
        event.setData(TaskManagerConfig.APPROVAL_FIELD_EVENT_VAR, column.getDataIndex());
        EventDispatcher.forwardEvent(event);
    }
}
