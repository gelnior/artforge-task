package fr.hd3d.taskmanager.ui.client.events;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * TaskManagerEvents contains all event code values sent to the event dispatcher by task manager view and model.
 * 
 * @author HD3D
 */
public class TaskManagerEvents
{
    public static final int START_EVENT = 16000;

    public static final EventType PROJECT_CHANGED = new EventType(START_EVENT + 6);
    public static final EventType CREATE_MENU_ASSIGNATION = new EventType(START_EVENT + 8);
    public static final EventType UPDATE_CELL_STATUS = new EventType(START_EVENT + 9);
    public static final EventType UPDATE_CELL_WORKER = new EventType(START_EVENT + 10);
    public static final EventType SHOW_NEW_TASK_WINDOW = new EventType(START_EVENT + 11);
    public static final EventType TASK_TYPE_BUTTON_CLICKED = new EventType();
    public static final EventType UPDATE_CELL_DURATION = new EventType();

    public static final EventType DELETE_TASK_CLICKED = new EventType();
    public static final EventType DELETE_TASK_CONFIRMED = new EventType();
    public static final EventType DELETE_TASK_SUCCESS = new EventType();
    public static final EventType SHEET_DELETED = new EventType();
    public static final EventType SHEET_CHANGED = new EventType();
    public static final EventType CREATE_NEW_TASK_CONSTITUENT_CLICKED = new EventType();
    public static final EventType CREATE_NEW_TASK_SHOT_CLICKED = new EventType();
    public static final EventType TASK_CREATED = new EventType();
    public static final EventType APPROVAL_DOUBLE_CLICKED = new EventType();
    public static final EventType PERSON_ASSIGNED = new EventType();

    public static final EventType UPDATE_CELL_STATUS_CONFIRMED = new EventType();
    public static final EventType ODS_EXPORT_CLICKED  = new EventType();
}
